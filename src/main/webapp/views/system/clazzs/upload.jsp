<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <script src="/admin/js/jquery.js"></script>

    <script>
        $(document).ready(function () {
            $("#sub").hide();
            //上传文件限制
            $("#excel").change(function () {
                var s = $("#excel").val();
                var start = s.indexOf(".") + 1;
                var name = s.substring(start, s.length).toLowerCase();//转成小写
                var filename = s.substring(s.lastIndexOf("\\") + 1, s.indexOf("."));//得到真实的名字
                if (name != "xlsx") {
                    alert("只能上传.xlsx文件");
                    $(this).val("");
                }
                if (name == "xlsx" && filename == "用户基本信息") {
                    $("#sub").show();

                } else {
                    alert("你上传的文件不是用户基本信息 请下载-用户基本信息表");
                    $(this).val("");
                    window.location.href = "/excel/用户基本信息.xlsx";//下载的路径
                }
            })})
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>导入学生</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>添加学生</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/clazz/uploadExcel"  method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">班级号</label>
                                <div class="col-sm-4">
                                    ${clazzid}
                                    <input type="text" value="${clazzid}" name="clazzid" maxlength="20" hidden>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div id="file-pretty">
                                    <div class="form-group">
                                        <label class="font-noraml">文件选择（单选）</label>
                                        <input type="file" class="form-control" id="excel" name="excel">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="sub">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--右侧部分结束-->
</div>
</body>
</html>