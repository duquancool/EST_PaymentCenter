<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>模块修改</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>模块修改</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/trainingModule/edit"  method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">模块名称</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="trainingModuleName" placeholder="模块名称" value="${trainingModule.trainingModuleName}" maxlength="20">
                                    <input type="hidden" class="form-control" name="trainingModuleId" value="${trainingModule.trainingModuleId}"  >
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label"  >模块状态</label>
                                <div class="col-sm-4">
                                    <input  type="radio"   name="trainingModuleIsUser"  placeholder="trainingModuleIsUser"   value="1"  <c:if test="${trainingModule.trainingModuleIsUser == 1}">checked="checked"</c:if>>可用
                                    <input  type="radio"   name="trainingModuleIsUser"  placeholder="trainingModuleIsUser"    value="0" <c:if test="${trainingModule.trainingModuleIsUser == 0}">checked="checked"</c:if>>禁用
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">修改</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--右侧部分结束-->
</div>
</body>
</html>