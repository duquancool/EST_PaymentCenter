<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <!--登陆人信息 后期可以直接跳转到 个人信息页面-->
                <a class="dropdown-toggle count-info"  href="#">
                    欢迎你,${sys_user_key.account}
                </a>
            </li>
            <li class="dropdown">
                <!--登陆人信息 后期可以直接跳转到 个人信息页面-->
                <a class="dropdown-toggle count-info" href="/system/logout">
                   退出
                </a>
            </li>
        </ul>
    </nav>
</div>