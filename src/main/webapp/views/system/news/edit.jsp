<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>新闻添加</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg">
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>修改新闻</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/news/edit"  method="post">
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="title"  placeholder="Title" maxlength="100" value="${info.title}">
                                    <input class="form-control" type="hidden" id="id" name="id" placeholder="id" maxlength="180"  value="${info.id}">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">Author</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="author" placeholder="Author" maxlength="30"  value="${info.author}">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">Summary</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="summary" placeholder="Summary" maxlength="500"  value="${info.summary}">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" for="content">Content</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" style="height:300px;visibility:hidden;" id="content" name="content">${info.content}</textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label"  >Status</label>
                                <div class="col-sm-4">
                                    <input  type="radio"   name="status"  placeholder="status"   value="1"   <c:if test="${info.status == 1}">checked="checked"</c:if>>发布
                                    <input  type="radio"   name="status"  placeholder="status"    value="0"  <c:if test="${info.status == 0}">checked="checked"</c:if>>草稿
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">修改</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--右侧部分结束-->
</div>
<script>
    /**引入富文本框**/
    var editor;
    KindEditor.ready(function(K) {
        //抓取对应的名称  转化为富文本
        editor = K.create('textarea[name="content"]', {
            allowFileManager : true
        });
    });
</script>
</body>
</html>