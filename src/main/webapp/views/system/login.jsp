<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/6
  Time: 15:23
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>集智教学平台后台 - 登录</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="taglib.jsp"></jsp:include>
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">集智</h1>
        </div>
        <h2>欢迎使用,教学平台后台</h2>
        <form class="m-t" role="form" action="login" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="account"  placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="密码" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">登 录</button>
            <%--<p class="text-muted text-center"> <a href="register.html">注册一个新账号</a></p>--%>
        </form>
    </div>
</div>
</body>
</html>





