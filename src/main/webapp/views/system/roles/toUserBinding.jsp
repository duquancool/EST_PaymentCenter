<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>/
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>已绑定人员列表</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>

<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>已绑定人员页面</h5>
                                <div class="ibox-tools">
                                        <button type="button" class="btn btn-w-m btn-danger  btn-sm" id="bindingUser">移除选择的人员</button>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>
                                                <input type="checkbox"  id="checkAll">全选
                                         </th>
                                        <th>人员编号</th>
                                        <th>人员名称</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${userList}" var="user">
                                        <tr class="gradeX">
                                            <td  class="center"> <input type="checkbox" name="userId"  value="${user.userId}"></td>
                                            <td  class="center">${user.userId}</td>
                                            <td  class="center">${user.userRealName}</td>
                                            <td  class="center">
                                                <a  href="/system/admin/role/removeBindingUser/${user.userId}/${roleId}"><button type="button" class="btn btn-w-m btn-danger  btn-sm">移除人员</button></a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--右侧部分结束-->
</div>
<!-- 标记页面的例子 -->
<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable();
    });

    /**
     * 全选方法
     */
    var i=0;//状态
    //全选
    $("#checkAll").on("click",function(){
        if(i==0){
            //把所有复选框选中
            $('input[name="userId"]').prop("checked", true);
            i=1;
        }else{
            $('input[name="userId"]').prop("checked", false);
            i=0;
        }
    });

    //执行绑定的方法
    $("#bindingUser").on("click",function(){
        //先判断是否选中菜单
        var  shu=$('input:checkbox[name="userId"]:checked').size();
        if(shu>0){
            //如果选中就执行
            var ids=new Array();
            $('input:checkbox[name="userId"]:checked').each(function(i){
                ids[i] = $(this).val();
            });
            var vals = ids.join("_");//已经选择的id集合信息
            $(location).attr('href',"/system/admin/role/removeBindingUsers/${roleId}/"+vals);//跳转到批量人员移除角色
        }
        else{
            alert("未选择要移除的人员");
        }
    });
</script>
</body>
</html>