<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>/
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>课程下模块列表</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>

<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>课程下模块列表</h5>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>模块名称</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${ctmList}" var="ctm">
                                        <tr class="gradeX">
                                            <td>${ctm.trainingModuleName}</td>
                                            <td  class="center">
                                                <a  href="/system/admin/courseModule/queryCompanyByTrainingModuleId/${ctm.courseTrainingModuleId}"><button type="button" class="btn btn-w-m btn-info  btn-sm">公司列表</button></a>
                                                <%--<a class="edit" href="/system/admin/course/toEdit/${course.courseId}"><button type="button" class="btn btn-w-m btn-warning  btn-sm">修改</button></a>&nbsp;|&nbsp;--%>
                                                <%--<a class="delete" href="/system/admin/course/delete/${course.courseId}"><button type="button" class="btn btn-w-m btn-danger  btn-sm">删除</button></a>--%>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--右侧部分结束-->
</div>
<!-- 标记页面的例子 -->
<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable();
    });
</script>
</body>
</html>