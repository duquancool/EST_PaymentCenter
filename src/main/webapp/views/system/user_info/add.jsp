<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <script src="/admin/js/jquery.js"></script>
    <script>

        $(document).ready(function(){
            $('#uploadImg').click(function(){
                $('#img-upload').click();
            });
            $("#img-upload").change(function(){
                $("#uploadImg").attr("src",URL.createObjectURL($(this)[0].files[0]));
            });
        });</script>
    <title>用户添加</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>添加班级</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/userInfo/add"  method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div style="height: 190px;width: 150px; margin: 0 25%" >
                                <img id="uploadImg" src="/upload/userinfo/笑脸.jpg" width="100%" height="85%" style="border-radius: 50%"border="1px solid black">
                                <input id="img-upload" type="file" name="realpath" style="display:none;">点击笑脸上传图片
                            </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">登录账号</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="登录账号" name="userName" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">密码</label>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" placeholder="password" name="userPassword" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">学号</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="学号" name="userStudentNumber" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">真实姓名</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="真实姓名" name="userRealname" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">性别</label>
                                <div class="col-sm-10">
                                    <label class="checkbox-inline">
                                        <input type="radio" value="1" id="inlineCheckbox1" name="userSex" checked>男</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="2" id="inlineCheckbox3" name="userSex">女</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">手机号</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" placeholder="手机号" name="userTelphone" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">邮箱</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control"placeholder="邮箱" name="userEmail"maxlength="20">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">使用状态</label>
                                <div class="col-sm-10">
                                    <label class="checkbox-inline">
                                        <input type="radio" value="1"  name="userIsUser" checked>可用</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" value="2"  name="userIsUser">不可用</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">有效访问时长</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control"placeholder="有效访问时长" name="userIsUser"maxlength="20">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">添加</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--右侧部分结束-->
</div>
</body>
</html>