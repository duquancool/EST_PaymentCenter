<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>用户信息修改</title>
    <script src="/admin/js/jquery.js"></script>
    <script>
        $(document).ready(function(){
            $('#uploadImg').click(function(){
                $('#img-upload').click();
            });
            $("#img-upload").change(function(){
                $("#uploadImg").attr("src",URL.createObjectURL($(this)[0].files[0]));
            });
        });</script>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg"  >
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>修改用户信息</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/userInfo/edit"  method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div style="height: 190px;width: 150px; margin: 0 25%" >
                                    <img id="uploadImg" src="${page.userImgurl}" width="100%" height="85%" style="border-radius: 50%"border="1px solid black">
                                    <input id="img-upload" type="file" name="realpath" style="display:none;">点击图片更改
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">用户账号</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control"  value="${page.userName}"   name="userName" maxlength="20">
                                    <input type="hidden" value="${page.userId}" name="userId">
                                    <input type="hidden" value="${page.userImgurl}" name="userImgurl">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">密码</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control"   value="${page.userPassword}" name="userPassword"  maxlength="20">
                                </div>
                            </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">用户名</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"   value="${page.userRealname}" name="userRealname"  maxlength="20">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">学号</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"   value="${page.userStudentNumber}" name="userStudentNumber"  maxlength="20">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">性别</label>
                                    <div class="col-sm-10">
                                        <label class="checkbox-inline">
                                            <input type="radio" value="1" id="inlineCheckbox1" name="userSex" <c:if test="${page.userSex eq 1}"> checked</c:if>>男</label>
                                        <label class="checkbox-inline">
                                            <input type="radio" value="2" id="inlineCheckbox3" name="userSex"<c:if test="${page.userSex eq 2}"> checked</c:if>>女</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">手机号</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"   value="${page.userTelphone}" name="userTelphone"  maxlength="20">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">邮箱</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"   value="${page.userEmail}" name="userEmail"  maxlength="20">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">使用状态</label>
                                    <div class="col-sm-4">
                                        <div class="col-sm-10">
                                            <label class="checkbox-inline">
                                                <input type="radio" value="1" id="inlineCheckbox5" name="userIsUser" <c:if test="${page.userIsUser eq '1'}">checked</c:if>>可用</label>
                                            <label class="checkbox-inline">
                                                <input type="radio" value="2" id="inlineCheckbox4" name="userIsUser"<c:if test="${page.userIsUser eq '2'}">checked</c:if> >不可用</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">使用时长</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"   value="${page.userIsDays}" name="userIsUser"  maxlength="20">
                                    </div>
                                </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">修改</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--右侧部分结束-->
</div>
</body>
</html>