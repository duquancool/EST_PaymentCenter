<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>增加课程</title>
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <!--资源 css js 引入公共页面 -->
    <jsp:include page="../taglib.jsp"></jsp:include>
</head>
<body class="fixed-sidebar full-height-layout gray-bg">
<div id="wrapper"  style="height: auto;">
    <!--左侧导航开始-->
    <jsp:include page="../menu.jsp"></jsp:include>
    <!--左侧导航结束-->
    <!--右侧部分开始-->
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <jsp:include page="../right.jsp"></jsp:include>
        <!--显示的表单页面-->
        <div class="row J_mainContent" id="content-main">
            <!--嵌入的页面信息-->
            <div class="col-sm-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>增加课程</h5>
                    </div>
                    <div class="ibox-content">
                        <form  class="form-horizontal" action="/system/admin/course/add"  method="post">

                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">课程名称</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"name="courseName" maxlength="20">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">课程类型</label>
                                <div class="col-sm-6">
                                    <select data-placeholder="选择课程类型..." class="form-control"tabindex="2" style="text-align: center" name="courseType">
                                        <option >选择课程类型</option>
                                        <option value="1" hassubinfo="true" >体育</option>
                                        <option value="2" hassubinfo="true">财经</option>
                                        <option value="3" hassubinfo="true">高铁</option>
                                        <option value="4" hassubinfo="true">军事</option>
                                        <option value="5" hassubinfo="true">人事</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">课程专业</label>
                                <div class="col-sm-6">
                                    <select data-placeholder="选择专业..." class="form-control" style="text-align: center" tabindex="2" name="courseProfessional">
                                        <option>选择专业</option>
                                        <option value="1" hassubinfo="true" >计算机网络</option>
                                        <option value="2" hassubinfo="true" >软件专业</option>
                                        <option value="3" hassubinfo="true" >汽修专业</option>
                                        <option value="4" hassubinfo="true" >新东方</option>
                                        <option value="5" hassubinfo="true" >厨师</option>
                                        <option value="6" hassubinfo="true" >塔吊</option>
                                        <option value="7" hassubinfo="true" >程序员</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">课程时长</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control"  name="courseHour"  maxlength="20" placeholder="分钟">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">授课人数</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="courseNumber"  maxlength="20">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label">课程状态</label>
                                <div class="col-sm-6">
                                    开课<input  type="radio" name="courseStatus"  value="1" checked>
                                    不开课 <input  type="radio"name="courseStatus"   value="0" >
                                </div>
                            </div>
                            <div class="form-group form-group-sm">

                                <label class="col-sm-2 control-label">授课开始时间</label>
                                <div class="col-sm-6">
                                    <input type="datetime-local" name="courseStartTime" class="form-control diff-textarea"  style="text-align: center">
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >课程描述</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control diff-textarea"name="courseDescribe"></textarea>

                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >课程介绍</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control diff-textarea"   name="courseIntroduce" ></textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >课程概括</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control diff-textarea"    name="courseSummary"></textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >教学目标</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" style="height:240px;visibility:hidden;" name="courseObjectives"></textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >教学计划</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" style="height:240px;visibility:hidden;"  name="courseProgramme"></textarea>
                                </div>
                            </div>
                            <div class="form-group form-group-sm">
                                <label class="col-sm-2 control-label" >预先知识</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" style="height:240px;visibility:hidden;"  name="courseKnowledge"></textarea>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">创建课程</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--右侧部分结束-->
</div>
<script>
    /**引入富文本框**/
    var editor;
    KindEditor.ready(function(K) {
        //抓取对应的名称  转化为富文本
        editor = K.create('textarea[name="courseKnowledge"]', {
            allowFileManager : true
        });
        editor = K.create('textarea[name="courseProgramme"]', {
            allowFileManager : true
        });
        editor = K.create('textarea[name="courseObjectives"]', {
            allowFileManager : true
        });
    });
</script>
</body>
</html>