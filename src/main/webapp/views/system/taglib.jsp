<%--
  Created by IntelliJ IDEA.
  User: Ry.Du
  Date: 2019/12/10
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<%@page isELIgnored="false"%>
<!--可以替换为公司ico -->
<link rel="shortcut icon" href="favicon.ico">
<link href="<%=request.getContextPath()%>/admin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="<%=request.getContextPath()%>/admin/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="<%=request.getContextPath()%>/admin/css/animate.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/admin/css/style.css?v=4.1.0" rel="stylesheet">
<!-- 全局js -->
<script src="<%=request.getContextPath()%>/admin/js/jquery.min.js?v=2.1.4"></script>
<script src="<%=request.getContextPath()%>/admin/js/bootstrap.min.js?v=3.3.6"></script>
<script src="<%=request.getContextPath()%>/admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<%=request.getContextPath()%>/admin/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<%=request.getContextPath()%>/admin/js/plugins/layer/layer.min.js"></script>
<!-- 自定义js -->
<script src="<%=request.getContextPath()%>/admin/js/hAdmin.js?v=4.1.0"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/index.js"></script>
<!-- 全局js -->

<!-- Data Tables -->
<link href="<%=request.getContextPath()%>/admin/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<!-- 全局js -->
<script src="<%=request.getContextPath()%>/admin/js/plugins/jeditable/jquery.jeditable.js"></script>
<script src="<%=request.getContextPath()%>/admin/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<%=request.getContextPath()%>/admin/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<!-- 自定义js -->
<script src="<%=request.getContextPath()%>/admin/js/content.js?v=1.0.0"></script>
<!--富文本 引入-->
<link rel="stylesheet" href="<%=request.getContextPath()%>/kindeditor/themes/default/default.css" />
<script charset="utf-8" src="<%=request.getContextPath()%>/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<%=request.getContextPath()%>/kindeditor/lang/zh_CN.js"></script>
