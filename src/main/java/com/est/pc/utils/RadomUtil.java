package com.est.pc.utils;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * @Function:
 * @author: Mr.Han
 * @create: 2019-12-19 10:44
 **/
public class RadomUtil {
    private static final char[] r=new char[]{'q', 'w', 'e', '8', 'a', 's', '2', 'd', 'z', 'x', '9', 'c', '7', 'p', '5', 'i', 'k', '3', 'm', 'j', 'u', 'f', 'r', '4', 'v', 'y', 'l', 't', 'n', '6', 'b', 'g', 'h'};
    public static String getRadom (){
        String Radom="";
        Random ran = new Random();
        Set<Integer> set = new TreeSet<>();
        while (true) {
            int a = ran.nextInt(33);
            set.add(a);
            if (set.size() >5) {
                break;
            }
        }
        for (Integer i:set) {
            Radom+=r[i];
        }
        return Radom;
    }
}
