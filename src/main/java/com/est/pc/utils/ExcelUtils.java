package com.est.pc.utils;

import com.est.pc.domain.UserInfoMiddle;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Function:
 * @author: Mr.Han
 * @create: 2019-12-12 15:47
 **/
@SuppressWarnings({"deprecation"})

public class ExcelUtils {
    public static Object execelToShopIdList(InputStream inputStream){
        List<UserInfoMiddle> list=new ArrayList<>();//接受文本的容器
        List<Map<String,Object>> nullList=new ArrayList<>(); //记录空值的容器
        Boolean flag=true;//一开始判断条件为真
        Workbook workbook=null;
        try {
            workbook= WorkbookFactory.create(inputStream);
            inputStream.close();//关闭资源
            Sheet sheet=workbook.getSheetAt(0);
            int rowlength = sheet.getLastRowNum();
            Row row = sheet.getRow(0);
            int lastCellNum = row.getLastCellNum();
            Cell cell=row.getCell(0);
            for(int i=1;i<=rowlength;i++){
                UserInfoMiddle userInfoMiddle=new UserInfoMiddle();
                Map<String,Object> Nulllist=new HashMap<>();
                row=sheet.getRow(i);
                for(int j=0;j<=lastCellNum;j++){
                    cell=row.getCell(j);
                    try{
                        cell.setCellType(Cell.CELL_TYPE_STRING);//接受String 类型
                        String data = cell.getStringCellValue();
                        data=data.trim();//去除空格
                        //System.out.println("第"+i+"行"+"第"+j+"列的数据"+data);
                        if(j==0){
                                userInfoMiddle.setUserName(data);
                        }else
                        if(j==1){
                            userInfoMiddle.setUserPassword(data);}
                        else if(j==2){

                                userInfoMiddle.setUserStudentNumber(Integer.valueOf(data).longValue());
                            }

                        if(j==3){
                                if(data!=""){
                                    userInfoMiddle.setUserRealname(data);
                                }else{
                                    Nulllist.put("UserRealname","第"+i+"行的用户姓名存在空值");
                                    flag=false;
                                }


                            }
                        else if(j==4){
                            if(data!=""){
                                if(data.equals("男")||data.equals("女")){
                                    if(data.equals("男")){
                                        data="1";
                                    }
                                    else if(data.equals("女")){
                                        data="2";
                                    }
                                    userInfoMiddle.setUserSex(data);}}else {
                                Nulllist.put("UserRealname","第"+i+"行的用户姓名存在空值");
                                flag=false;
                            }

                        }
                        if(j==5){userInfoMiddle.setUserImgurl(data);}
                        else if(j==6){
                            //System.out.println(data);

                            userInfoMiddle.setUserTelphone(Long.valueOf(data));


                        }else
                        if(j==7){userInfoMiddle.setUserEmail(data);}else
                        if(j==8){userInfoMiddle.setUserIsDays(Integer.valueOf(data).shortValue());}
                    }
                    catch (Exception e){
                       if(j==0){

                                Nulllist.put("UserName","第"+i+"行的用户账号为空值");
                                flag=false;
                            }
                        else if(j==2){

                                Nulllist.put("UserStudentNumber","第"+i+"行的学号格式不对存在字母或空值");
                                flag=false;
                            }else if(j==4){
                            Nulllist.put("UserSex","第"+i+"行的性别格式不对存在字母或空值");
                       }


                        else if(j==6){


                            Nulllist.put("UserTelphone","第"+i+"行的电话号存在字母或空值");

                        }
                }
                }
                if(flag){
                    list.add(userInfoMiddle);
                }else{
                    nullList.add(Nulllist);//保存空值结果
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        if (flag)return list;
            else
                return nullList;

    }

}
