package com.est.pc.domain;

import java.util.Date;
/**
 * @Author:RyDu
 * @Date:2019/12/20 17:18
 * @Function:公司组织结构表
 */
public class CorporateOrganizationalStructure {
    private Long corporateOrganizationalStructureId;//公司组织结构编号
    private Long courseTrainingModuleId;//课程模块关联编号
    private String corporateOrganizationalStructureName;//公司组织名称
    private Date corporateOrganizationalStructureCreatetime;//创建时间
    private Date corporateOrganizationalStructureModifiedtime;//修改时间

    public Long getCorporateOrganizationalStructureId() {
        return corporateOrganizationalStructureId;
    }

    public void setCorporateOrganizationalStructureId(Long corporateOrganizationalStructureId) {
        this.corporateOrganizationalStructureId = corporateOrganizationalStructureId;
    }

    public Long getCourseTrainingModuleId() {
        return courseTrainingModuleId;
    }

    public void setCourseTrainingModuleId(Long courseTrainingModuleId) {
        this.courseTrainingModuleId = courseTrainingModuleId;
    }

    public String getCorporateOrganizationalStructureName() {
        return corporateOrganizationalStructureName;
    }

    public void setCorporateOrganizationalStructureName(String corporateOrganizationalStructureName) {
        this.corporateOrganizationalStructureName = corporateOrganizationalStructureName;
    }

    public Date getCorporateOrganizationalStructureCreatetime() {
        return corporateOrganizationalStructureCreatetime;
    }

    public void setCorporateOrganizationalStructureCreatetime(Date corporateOrganizationalStructureCreatetime) {
        this.corporateOrganizationalStructureCreatetime = corporateOrganizationalStructureCreatetime;
    }

    public Date getCorporateOrganizationalStructureModifiedtime() {
        return corporateOrganizationalStructureModifiedtime;
    }

    public void setCorporateOrganizationalStructureModifiedtime(Date corporateOrganizationalStructureModifiedtime) {
        this.corporateOrganizationalStructureModifiedtime = corporateOrganizationalStructureModifiedtime;
    }
}