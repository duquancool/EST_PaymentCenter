package com.est.pc.domain;

import java.util.Date;
/**
 * @Author:Han
 * @Date:2019/12/19
 * @Function:课程表
 */
public class Course {
    private Long etCourseId;
    private Long courseId;
    private String courseName;

    private String courseType;//课程类型    //下拉框

    private String courseProfessional;//课程专业 //下拉框

    private Short courseHour;

    private Short courseNumber;

    private String courseStatus;//课程状态
    private Date courseStartTime;//开始时间

    private Date courseEndTime;//结束时间  //后台计算 也有可能发生变动

    private String courseKey;//邀请码

    private String courseDescribe;//课程描述

    private String courseIntroduce;//课程介绍

    private String courseSummary;//课程概括

    private String courseObjectives;//教学目标

    private String courseProgramme;//教学计划

    private String courseKnowledge;//预先知识

    private Date courseCreate;

    private Date courseModified;

    public Long getEtCourseId() {
        return etCourseId;
    }

    public void setEtCourseId(Long etCourseId) {
        this.etCourseId = etCourseId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getCourseProfessional() {
        return courseProfessional;
    }

    public void setCourseProfessional(String courseProfessional) {
        this.courseProfessional = courseProfessional;
    }

    public Short getCourseHour() {
        return courseHour;
    }

    public void setCourseHour(Short courseHour) {
        this.courseHour = courseHour;
    }

    public Short getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(Short courseNumber) {
        this.courseNumber = courseNumber;
    }

    public String getCourseStatus() {
        return courseStatus;
    }

    public void setCourseStatus(String courseStatus) {
        this.courseStatus = courseStatus;
    }

    public Date getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(Date courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

    public Date getCourseEndTime() {
        return courseEndTime;
    }

    public void setCourseEndTime(Date courseEndTime) {
        this.courseEndTime = courseEndTime;
    }

    public String getCourseKey() {
        return courseKey;
    }

    public void setCourseKey(String courseKey) {
        this.courseKey = courseKey;
    }

    public String getCourseDescribe() {
        return courseDescribe;
    }

    public void setCourseDescribe(String courseDescribe) {
        this.courseDescribe = courseDescribe;
    }

    public String getCourseIntroduce() {
        return courseIntroduce;
    }

    public void setCourseIntroduce(String courseIntroduce) {
        this.courseIntroduce = courseIntroduce;
    }

    public String getCourseSummary() {
        return courseSummary;
    }

    public void setCourseSummary(String courseSummary) {
        this.courseSummary = courseSummary;
    }

    public String getCourseObjectives() {
        return courseObjectives;
    }

    public void setCourseObjectives(String courseObjectives) {
        this.courseObjectives = courseObjectives;
    }

    public String getCourseProgramme() {
        return courseProgramme;
    }

    public void setCourseProgramme(String courseProgramme) {
        this.courseProgramme = courseProgramme;
    }

    public String getCourseKnowledge() {
        return courseKnowledge;
    }

    public void setCourseKnowledge(String courseKnowledge) {
        this.courseKnowledge = courseKnowledge;
    }

    public Date getCourseCreate() {
        return courseCreate;
    }

    public void setCourseCreate(Date courseCreate) {
        this.courseCreate = courseCreate;
    }

    public Date getCourseModified() {
        return courseModified;
    }

    public void setCourseModified(Date courseModified) {
        this.courseModified = courseModified;
    }

    @Override
    public String toString() {
        return "Course{" +
                "etCourseId=" + etCourseId +
                ", courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseType='" + courseType + '\'' +
                ", courseProfessional='" + courseProfessional + '\'' +
                ", courseHour=" + courseHour +
                ", courseNumber=" + courseNumber +
                ", courseStatus='" + courseStatus + '\'' +
                ", courseStartTime=" + courseStartTime +
                ", courseEndTime=" + courseEndTime +
                ", courseKey='" + courseKey + '\'' +
                ", courseDescribe='" + courseDescribe + '\'' +
                ", courseIntroduce='" + courseIntroduce + '\'' +
                ", courseSummary='" + courseSummary + '\'' +
                ", courseObjectives='" + courseObjectives + '\'' +
                ", courseProgramme='" + courseProgramme + '\'' +
                ", courseKnowledge='" + courseKnowledge + '\'' +
                ", courseCreate=" + courseCreate +
                ", courseModified=" + courseModified +
                '}';
    }
}