package com.est.pc.domain;

import java.io.Serializable;
import java.util.Date;
/**
 * @Author:RyDu
 * @Date:2019/12/06 15:42
 * @Function:角色表
 */
public class RoleInfo implements Serializable {
    private Long roleid;//角色id
    private String rolename;//角色名
    private Date rolecreatetime;//创建时间
    private Date rolemodifiedtime;//修改时间

    public Long getRoleid() {
        return roleid;
    }

    public void setRoleid(Long roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public Date getRolecreatetime() {
        return rolecreatetime;
    }

    public void setRolecreatetime(Date rolecreatetime) {
        this.rolecreatetime = rolecreatetime;
    }

    public Date getRolemodifiedtime() {
        return rolemodifiedtime;
    }

    public void setRolemodifiedtime(Date rolemodifiedtime) {
        this.rolemodifiedtime = rolemodifiedtime;
    }
}
