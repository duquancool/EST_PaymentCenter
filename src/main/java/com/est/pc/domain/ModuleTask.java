package com.est.pc.domain;

import java.io.Serializable;
import java.util.Date;

public class ModuleTask implements Serializable {
    private Long moduleTaskId;

    private Long trainingModuleId;

    private String moduleTaskSection;

    private String moduleTaskLogoUrl;

    private String moduleTaskName;

    private Boolean moduleTaskType;

    private String moduleTaskDescription;

    private Boolean moduleTaskStatus;

    private Byte moduleTaskScore;

    private Byte moduleTaskTimes;

    private Date moduleTaskCreatetime;

    private Date moduleTaskModifiedtime;

    public Long getModuleTaskId() {
        return moduleTaskId;
    }

    public void setModuleTaskId(Long moduleTaskId) {
        this.moduleTaskId = moduleTaskId;
    }

    public Long getTrainingModuleId() {
        return trainingModuleId;
    }

    public void setTrainingModuleId(Long trainingModuleId) {
        this.trainingModuleId = trainingModuleId;
    }

    public String getModuleTaskSection() {
        return moduleTaskSection;
    }

    public void setModuleTaskSection(String moduleTaskSection) {
        this.moduleTaskSection = moduleTaskSection;
    }

    public String getModuleTaskLogoUrl() {
        return moduleTaskLogoUrl;
    }

    public void setModuleTaskLogoUrl(String moduleTaskLogoUrl) {
        this.moduleTaskLogoUrl = moduleTaskLogoUrl;
    }

    public String getModuleTaskName() {
        return moduleTaskName;
    }

    public void setModuleTaskName(String moduleTaskName) {
        this.moduleTaskName = moduleTaskName;
    }

    public Boolean getModuleTaskType() {
        return moduleTaskType;
    }

    public void setModuleTaskType(Boolean moduleTaskType) {
        this.moduleTaskType = moduleTaskType;
    }

    public String getModuleTaskDescription() {
        return moduleTaskDescription;
    }

    public void setModuleTaskDescription(String moduleTaskDescription) {
        this.moduleTaskDescription = moduleTaskDescription;
    }

    public Boolean getModuleTaskStatus() {
        return moduleTaskStatus;
    }

    public void setModuleTaskStatus(Boolean moduleTaskStatus) {
        this.moduleTaskStatus = moduleTaskStatus;
    }

    public Byte getModuleTaskScore() {
        return moduleTaskScore;
    }

    public void setModuleTaskScore(Byte moduleTaskScore) {
        this.moduleTaskScore = moduleTaskScore;
    }

    public Byte getModuleTaskTimes() {
        return moduleTaskTimes;
    }

    public void setModuleTaskTimes(Byte moduleTaskTimes) {
        this.moduleTaskTimes = moduleTaskTimes;
    }

    public Date getModuleTaskCreatetime() {
        return moduleTaskCreatetime;
    }

    public void setModuleTaskCreatetime(Date moduleTaskCreatetime) {
        this.moduleTaskCreatetime = moduleTaskCreatetime;
    }

    public Date getModuleTaskModifiedtime() {
        return moduleTaskModifiedtime;
    }

    public void setModuleTaskModifiedtime(Date moduleTaskModifiedtime) {
        this.moduleTaskModifiedtime = moduleTaskModifiedtime;
    }
}