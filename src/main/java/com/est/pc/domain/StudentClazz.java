package com.est.pc.domain;

import java.io.Serializable;
import java.util.Date;
/**
 * @Author:Han
 * @Date:2019/12/18 9:24
 * @Function:班级中间表
 */
public class StudentClazz implements Serializable {
    private Long studentClazzId;

    private Long studentId;

    private Long teacherId;

    private Long clazzId;

    private Date studentClazzCreatetime;

    private Date studentClazzModifiedtime;

    public Long getStudentClazzId() {
        return studentClazzId;
    }

    public void setStudentClazzId(Long studentClazzId) {
        this.studentClazzId = studentClazzId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getClazzId() {
        return clazzId;
    }

    public void setClazzId(Long clazzId) {
        this.clazzId = clazzId;
    }

    public Date getStudentClazzCreatetime() {
        return studentClazzCreatetime;
    }

    public void setStudentClazzCreatetime(Date studentClazzCreatetime) {
        this.studentClazzCreatetime = studentClazzCreatetime;
    }

    public Date getStudentClazzModifiedtime() {
        return studentClazzModifiedtime;
    }

    public void setStudentClazzModifiedtime(Date studentClazzModifiedtime) {
        this.studentClazzModifiedtime = studentClazzModifiedtime;
    }
}