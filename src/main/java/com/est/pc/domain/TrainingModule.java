package com.est.pc.domain;

import java.util.Date;

public class TrainingModule {
    private Long trainingModuleId;//模块编号
    private String trainingModuleName;//模块名称
    private Integer trainingModuleIsUser;//使用状态
    private Date trainingModuleCreatetime;//创建时间
    private Date trainingModuleModifiedtime;//修改时间


    public Long getTrainingModuleId() {
        return trainingModuleId;
    }

    public void setTrainingModuleId(Long trainingModuleId) {
        this.trainingModuleId = trainingModuleId;
    }

    public String getTrainingModuleName() {
        return trainingModuleName;
    }

    public void setTrainingModuleName(String trainingModuleName) {
        this.trainingModuleName = trainingModuleName;
    }

    public Integer getTrainingModuleIsUser() {
        return trainingModuleIsUser;
    }

    public void setTrainingModuleIsUser(Integer trainingModuleIsUser) {
        this.trainingModuleIsUser = trainingModuleIsUser;
    }

    public Date getTrainingModuleCreatetime() {
        return trainingModuleCreatetime;
    }

    public void setTrainingModuleCreatetime(Date trainingModuleCreatetime) {
        this.trainingModuleCreatetime = trainingModuleCreatetime;
    }

    public Date getTrainingModuleModifiedtime() {
        return trainingModuleModifiedtime;
    }

    public void setTrainingModuleModifiedtime(Date trainingModuleModifiedtime) {
        this.trainingModuleModifiedtime = trainingModuleModifiedtime;
    }
}