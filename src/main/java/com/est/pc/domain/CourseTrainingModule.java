package com.est.pc.domain;

import java.util.Date;
/**
 * @Author:RyDu
 * @Date:2019/12/20 15:33
 * @Function:课程模块表
 */
public class CourseTrainingModule {
    private Long courseTrainingModuleId;
    private Long trainingModuleId;
    private Long courseId;
    private Integer courseTrainingModuleStatus;
    private String courseTrainingModuleKeys;
    private Date courseTrainingModuleCreatetime;
    private Date courseTrainingModuleModifiedtime;

    //联表显示模块名称
    private String trainingModuleName;//模块名称

    public Long getCourseTrainingModuleId() {
        return courseTrainingModuleId;
    }

    public void setCourseTrainingModuleId(Long courseTrainingModuleId) {
        this.courseTrainingModuleId = courseTrainingModuleId;
    }

    public Long getTrainingModuleId() {
        return trainingModuleId;
    }

    public void setTrainingModuleId(Long trainingModuleId) {
        this.trainingModuleId = trainingModuleId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Integer getCourseTrainingModuleStatus() {
        return courseTrainingModuleStatus;
    }

    public void setCourseTrainingModuleStatus(Integer courseTrainingModuleStatus) {
        this.courseTrainingModuleStatus = courseTrainingModuleStatus;
    }

    public String getCourseTrainingModuleKeys() {
        return courseTrainingModuleKeys;
    }

    public void setCourseTrainingModuleKeys(String courseTrainingModuleKeys) {
        this.courseTrainingModuleKeys = courseTrainingModuleKeys;
    }

    public Date getCourseTrainingModuleCreatetime() {
        return courseTrainingModuleCreatetime;
    }

    public void setCourseTrainingModuleCreatetime(Date courseTrainingModuleCreatetime) {
        this.courseTrainingModuleCreatetime = courseTrainingModuleCreatetime;
    }

    public Date getCourseTrainingModuleModifiedtime() {
        return courseTrainingModuleModifiedtime;
    }

    public void setCourseTrainingModuleModifiedtime(Date courseTrainingModuleModifiedtime) {
        this.courseTrainingModuleModifiedtime = courseTrainingModuleModifiedtime;
    }

    public String getTrainingModuleName() {
        return trainingModuleName;
    }

    public void setTrainingModuleName(String trainingModuleName) {
        this.trainingModuleName = trainingModuleName;
    }
}