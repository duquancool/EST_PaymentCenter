package com.est.pc.domain;

import java.util.Date;
/**
 * @Function:角色中间表模板
 * @author: Mr.Han
 * @create: 2019-12-11 13:42
 **/
public class UserRole {
    private Long userRoleId;//用户角色关联编号
    private Long roleId;//角色编号
    private Long userId;//用户编号
    private Date userRoleCreatetime;//创建时间
    private Date userRoleModifiedtime;//修改时间

    //关联查询 角色名称  与 用户名称进行 展示
    private String userRealName;//真实姓名
    private String roleName;//角色名


    public Long getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(Long userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getUserRoleCreatetime() {
        return userRoleCreatetime;
    }

    public void setUserRoleCreatetime(Date userRoleCreatetime) {
        this.userRoleCreatetime = userRoleCreatetime;
    }

    public Date getUserRoleModifiedtime() {
        return userRoleModifiedtime;
    }

    public void setUserRoleModifiedtime(Date userRoleModifiedtime) {
        this.userRoleModifiedtime = userRoleModifiedtime;
    }

    public String getUserRealName() {
        return userRealName;
    }

    public void setUserRealName(String userRealName) {
        this.userRealName = userRealName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}