package com.est.pc.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @Function:资源表
 * @author: Mr.Han
 * @create: 2019-12-09 11:06
 **/
public class Resources implements Serializable {
    private  Long resourceid;//资源编号
    private  byte resourcetype;//资源分类
    private byte  coursetype;//课节分类
    private String resourcetitle;//资源标题
    private String  resourceurl;//资源路径
    private String  resourceanalyze;//资源分析
    private byte resourceisuser;//资源学生显示
    private Date resourcecreatetime;//创建时间
    private Date resourcemodifiedtime;//修改时间

    public Long getResourceid() {
        return resourceid;
    }

    public void setResourceid(Long resourceid) {
        this.resourceid = resourceid;
    }

    public byte getResourcetype() {
        return resourcetype;
    }

    public void setResourcetype(byte resourcetype) {
        this.resourcetype = resourcetype;
    }

    public byte getCoursetype() {
        return coursetype;
    }

    public void setCoursetype(byte coursetype) {
        this.coursetype = coursetype;
    }

    public String getResourcetitle() {
        return resourcetitle;
    }

    public void setResourcetitle(String resourcetitle) {
        this.resourcetitle = resourcetitle;
    }

    public String getResourceurl() {
        return resourceurl;
    }

    public void setResourceurl(String resourceurl) {
        this.resourceurl = resourceurl;
    }

    public String getResourceanalyze() {
        return resourceanalyze;
    }

    public void setResourceanalyze(String resourceanalyze) {
        this.resourceanalyze = resourceanalyze;
    }

    public byte getResourceisuser() {
        return resourceisuser;
    }

    public void setResourceisuser(byte resourceisuser) {
        this.resourceisuser = resourceisuser;
    }

    public Date getResourcecreatetime() {
        return resourcecreatetime;
    }

    public void setResourcecreatetime(Date resourcecreatetime) {
        this.resourcecreatetime = resourcecreatetime;
    }

    public Date getResourcemodifiedtime() {
        return resourcemodifiedtime;
    }

    public void setResourcemodifiedtime(Date resourcemodifiedtime) {
        this.resourcemodifiedtime = resourcemodifiedtime;
    }
}
