package com.est.pc.domain;

import java.util.Date;
/**
 * @Author:Han
 * @Date:2019/12/19
 * @Function:考评表封装类
 */
public class EvaluationSet {
    private Long ecourseId;//课程表传递的主键
    private Byte evaluationSetId;//考评编号

    private Long courseId;//课程编号

    private Byte evaluationSetCheckingIn;//考勤

    private String evaluationSetCheckingExplaining;//考勤说明

    private Byte evaluationSetFinishHomework;//完成作业

    private String evaluationSetHomeworkExplaining;//完成作业说明

    private Byte evaluationSetInspectionReport;//考核报告

    private String evaluationSetInspectionExplaining;//考核报告说明

    private Byte evaluationSetPostAbility;//岗位能力

    private String evaluationSetPostExplaining;//岗位能力说明

    private Byte evaluationSetWealthScore;//财富得分

    private String evaluationSetWealthExplaining;//财富得分说明

    private Byte evaluationSetProfessionalProficiency;//业务能力

    private String evaluationSetProfessionalExplaining;//业务能力说明

    private Byte evaluationSetComprehensiveEvaluation;//综合评分

    private String evaluationSetComprehensiveExplaining;//

    private Date evaluationSetCreatetime;//创建时间

    private Date evaluationSetModifiedtime;//修改时间

    public Long getEcourseId() {
        return ecourseId;
    }

    public void setEcourseId(Long ecourseId) {
        this.ecourseId = ecourseId;
    }

    public Byte getEvaluationSetId() {
        return evaluationSetId;
    }

    public void setEvaluationSetId(Byte evaluationSetId) {
        this.evaluationSetId = evaluationSetId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Byte getEvaluationSetCheckingIn() {
        return evaluationSetCheckingIn;
    }

    public void setEvaluationSetCheckingIn(Byte evaluationSetCheckingIn) {
        this.evaluationSetCheckingIn = evaluationSetCheckingIn;
    }

    public String getEvaluationSetCheckingExplaining() {
        return evaluationSetCheckingExplaining;
    }

    public void setEvaluationSetCheckingExplaining(String evaluationSetCheckingExplaining) {
        this.evaluationSetCheckingExplaining = evaluationSetCheckingExplaining;
    }

    public Byte getEvaluationSetFinishHomework() {
        return evaluationSetFinishHomework;
    }

    public void setEvaluationSetFinishHomework(Byte evaluationSetFinishHomework) {
        this.evaluationSetFinishHomework = evaluationSetFinishHomework;
    }

    public String getEvaluationSetHomeworkExplaining() {
        return evaluationSetHomeworkExplaining;
    }

    public void setEvaluationSetHomeworkExplaining(String evaluationSetHomeworkExplaining) {
        this.evaluationSetHomeworkExplaining = evaluationSetHomeworkExplaining;
    }

    public Byte getEvaluationSetInspectionReport() {
        return evaluationSetInspectionReport;
    }

    public void setEvaluationSetInspectionReport(Byte evaluationSetInspectionReport) {
        this.evaluationSetInspectionReport = evaluationSetInspectionReport;
    }

    public String getEvaluationSetInspectionExplaining() {
        return evaluationSetInspectionExplaining;
    }

    public void setEvaluationSetInspectionExplaining(String evaluationSetInspectionExplaining) {
        this.evaluationSetInspectionExplaining = evaluationSetInspectionExplaining;
    }

    public Byte getEvaluationSetPostAbility() {
        return evaluationSetPostAbility;
    }

    public void setEvaluationSetPostAbility(Byte evaluationSetPostAbility) {
        this.evaluationSetPostAbility = evaluationSetPostAbility;
    }

    public String getEvaluationSetPostExplaining() {
        return evaluationSetPostExplaining;
    }

    public void setEvaluationSetPostExplaining(String evaluationSetPostExplaining) {
        this.evaluationSetPostExplaining = evaluationSetPostExplaining;
    }

    public Byte getEvaluationSetWealthScore() {
        return evaluationSetWealthScore;
    }

    public void setEvaluationSetWealthScore(Byte evaluationSetWealthScore) {
        this.evaluationSetWealthScore = evaluationSetWealthScore;
    }

    public String getEvaluationSetWealthExplaining() {
        return evaluationSetWealthExplaining;
    }

    public void setEvaluationSetWealthExplaining(String evaluationSetWealthExplaining) {
        this.evaluationSetWealthExplaining = evaluationSetWealthExplaining;
    }

    public Byte getEvaluationSetProfessionalProficiency() {
        return evaluationSetProfessionalProficiency;
    }

    public void setEvaluationSetProfessionalProficiency(Byte evaluationSetProfessionalProficiency) {
        this.evaluationSetProfessionalProficiency = evaluationSetProfessionalProficiency;
    }

    public String getEvaluationSetProfessionalExplaining() {
        return evaluationSetProfessionalExplaining;
    }

    public void setEvaluationSetProfessionalExplaining(String evaluationSetProfessionalExplaining) {
        this.evaluationSetProfessionalExplaining = evaluationSetProfessionalExplaining;
    }

    public Byte getEvaluationSetComprehensiveEvaluation() {
        return evaluationSetComprehensiveEvaluation;
    }

    public void setEvaluationSetComprehensiveEvaluation(Byte evaluationSetComprehensiveEvaluation) {
        this.evaluationSetComprehensiveEvaluation = evaluationSetComprehensiveEvaluation;
    }

    public String getEvaluationSetComprehensiveExplaining() {
        return evaluationSetComprehensiveExplaining;
    }

    public void setEvaluationSetComprehensiveExplaining(String evaluationSetComprehensiveExplaining) {
        this.evaluationSetComprehensiveExplaining = evaluationSetComprehensiveExplaining;
    }

    public Date getEvaluationSetCreatetime() {
        return evaluationSetCreatetime;
    }

    public void setEvaluationSetCreatetime(Date evaluationSetCreatetime) {
        this.evaluationSetCreatetime = evaluationSetCreatetime;
    }

    public Date getEvaluationSetModifiedtime() {
        return evaluationSetModifiedtime;
    }

    public void setEvaluationSetModifiedtime(Date evaluationSetModifiedtime) {
        this.evaluationSetModifiedtime = evaluationSetModifiedtime;
    }

    @Override
    public String toString() {
        return "EvaluationSet{" +
                "ecourseId=" + ecourseId +
                ", evaluationSetId=" + evaluationSetId +
                ", courseId=" + courseId +
                ", evaluationSetCheckingIn=" + evaluationSetCheckingIn +
                ", evaluationSetCheckingExplaining='" + evaluationSetCheckingExplaining + '\'' +
                ", evaluationSetFinishHomework=" + evaluationSetFinishHomework +
                ", evaluationSetHomeworkExplaining='" + evaluationSetHomeworkExplaining + '\'' +
                ", evaluationSetInspectionReport=" + evaluationSetInspectionReport +
                ", evaluationSetInspectionExplaining='" + evaluationSetInspectionExplaining + '\'' +
                ", evaluationSetPostAbility=" + evaluationSetPostAbility +
                ", evaluationSetPostExplaining='" + evaluationSetPostExplaining + '\'' +
                ", evaluationSetWealthScore=" + evaluationSetWealthScore +
                ", evaluationSetWealthExplaining='" + evaluationSetWealthExplaining + '\'' +
                ", evaluationSetProfessionalProficiency=" + evaluationSetProfessionalProficiency +
                ", evaluationSetProfessionalExplaining='" + evaluationSetProfessionalExplaining + '\'' +
                ", evaluationSetComprehensiveEvaluation=" + evaluationSetComprehensiveEvaluation +
                ", evaluationSetComprehensiveExplaining='" + evaluationSetComprehensiveExplaining + '\'' +
                ", evaluationSetCreatetime=" + evaluationSetCreatetime +
                ", evaluationSetModifiedtime=" + evaluationSetModifiedtime +
                '}';
    }
}