package com.est.pc.domain;

import java.util.Date;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:13
 * @Function:
 */
public class Labelss {
    private Long labelsId;//标签编号

    private String labelsContent;//标签信息

    private Date labelsCreatetime;//创建时间

    private Date labelsModifiedtime;//修改时间

    public Long getLabelsId() {
        return labelsId;
    }

    public void setLabelsId(Long labelsId) {
        this.labelsId = labelsId;
    }

    public String getLabelsContent() {
        return labelsContent;
    }

    public void setLabelsContent(String labelsContent) {
        this.labelsContent = labelsContent;
    }

    public Date getLabelsCreatetime() {
        return labelsCreatetime;
    }

    public void setLabelsCreatetime(Date labelsCreatetime) {
        this.labelsCreatetime = labelsCreatetime;
    }

    public Date getLabelsModifiedtime() {
        return labelsModifiedtime;
    }

    public void setLabelsModifiedtime(Date labelsModifiedtime) {
        this.labelsModifiedtime = labelsModifiedtime;
    }
}
