package com.est.pc.domain;

import java.util.Date;

public class RoleMenus {
    private Long roleMenuId;//关联ID
    private Long menuId;//菜单ID
    private Long roleId;//角色ID
    private Date roleMenusCreatetime;//创建时间
    private Date roleMenusModifiedtime;//修改时间

    //关联信息
    private String roleName;//角色名称
    private String menuName;//菜单名称


    public Long getRoleMenuId() {
        return roleMenuId;
    }

    public void setRoleMenuId(Long roleMenuId) {
        this.roleMenuId = roleMenuId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Date getRoleMenusCreatetime() {
        return roleMenusCreatetime;
    }

    public void setRoleMenusCreatetime(Date roleMenusCreatetime) {
        this.roleMenusCreatetime = roleMenusCreatetime;
    }

    public Date getRoleMenusModifiedtime() {
        return roleMenusModifiedtime;
    }

    public void setRoleMenusModifiedtime(Date roleMenusModifiedtime) {
        this.roleMenusModifiedtime = roleMenusModifiedtime;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}