package com.est.pc.domain;

import java.io.Serializable;
import java.util.Date;
/**
 * @Author:Han
 * @Date:2019/12/07 13:42
 * @Function:班级表
 */
public class ClazzInfo implements Serializable {
    private  Long clazzid;//账号
    private String clazzname;//班级名称
    private int clazznumber;//班级号
    private int clazzlabelnumber;//班级标签号
    private String clazzisuser;//班级状态
    private Date clazzcreatetime;//班級創立時間
    private Date clazzmodifiedtime;//班级修改时间

    public Long getClazzid() {
        return clazzid;
    }

    public void setClazzid(Long clazzid) {
        this.clazzid = clazzid;
    }

    public String getClazzname() {
        return clazzname;
    }

    public void setClazzname(String clazzname) {
        this.clazzname = clazzname;
    }

    public int getClazznumber() {
        return clazznumber;
    }

    public void setClazznumber(int clazznumber) {
        this.clazznumber = clazznumber;
    }

    public int getClazzlabelnumber() {
        return clazzlabelnumber;
    }

    public void setClazzlabelnumber(int clazzlabelnumber) {
        this.clazzlabelnumber = clazzlabelnumber;
    }

    public String getClazzisuser() {
        return clazzisuser;
    }

    public void setClazzisuser(String clazzisuser) {
        this.clazzisuser = clazzisuser;
    }

    public Date getClazzcreatetime() {
        return clazzcreatetime;
    }

    public void setClazzcreatetime(Date clazzcreatetime) {
        this.clazzcreatetime = clazzcreatetime;
    }

    public Date getClazzmodifiedtime() {
        return clazzmodifiedtime;
    }

    public void setClazzmodifiedtime(Date clazzmodifiedtime) {
        this.clazzmodifiedtime = clazzmodifiedtime;
    }

    @Override
    public String toString() {
        return "ClazzInfo{" +
                "clazzid=" + clazzid +
                ", clazzname='" + clazzname + '\'' +
                ", clazznumber=" + clazznumber +
                ", clazzlabelnumber=" + clazzlabelnumber +
                ", clazzisuser='" + clazzisuser + '\'' +
                ", clazzcreatetime=" + clazzcreatetime +
                ", clazzmodifiedtime=" + clazzmodifiedtime +
                '}';
    }
}
