package com.est.pc.domain;

import java.util.Date;

public class Menus {
    private Long menuId;//菜单编号
    private String menuName;//菜单名称
    private Long menuPid;//菜单父节点
    private String menuUrl;//菜单地址
    private String menuDescription;//菜单描述
    private Byte menuStatus;//菜单状态
    private Date menuCreatetime;//创建时间
    private Date menuModifiedtime;//修改时间

    private String menuPName;//菜单父名称

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Long getMenuPid() {
        return menuPid;
    }

    public void setMenuPid(Long menuPid) {
        this.menuPid = menuPid;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public String getMenuDescription() {
        return menuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        this.menuDescription = menuDescription;
    }

    public Byte getMenuStatus() {
        return menuStatus;
    }

    public void setMenuStatus(Byte menuStatus) {
        this.menuStatus = menuStatus;
    }

    public Date getMenuCreatetime() {
        return menuCreatetime;
    }

    public void setMenuCreatetime(Date menuCreatetime) {
        this.menuCreatetime = menuCreatetime;
    }

    public Date getMenuModifiedtime() {
        return menuModifiedtime;
    }

    public void setMenuModifiedtime(Date menuModifiedtime) {
        this.menuModifiedtime = menuModifiedtime;
    }

    public String getMenuPName() {
        return menuPName;
    }

    public void setMenuPName(String menuPName) {
        this.menuPName = menuPName;
    }
}