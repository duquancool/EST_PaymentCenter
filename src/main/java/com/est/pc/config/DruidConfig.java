package com.est.pc.config;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Druid 数据库连接池
 * http://localhost:9000/druid/index.html 本地环境监控
 * @SuppressWarnings("unchecked")  去掉 compile 编译 安全问题
 */

@Configuration
@SuppressWarnings("unchecked")
public class DruidConfig {
	/**
	 * ServletRegistrationBean,
	 * @see com.alibaba.druid.support.http.ResourceServlet
	 * @return
	 */
	@Bean
	public ServletRegistrationBean statViewServlet() {
		//ServletRegistrationBean 可参考链接  介绍的是SpringBoot 支持Servlet以及JSP的东西
		//https://blog.csdn.net/weixin_39793752/article/details/89327924
		ServletRegistrationBean druid = new ServletRegistrationBean();
		druid.setServlet(new StatViewServlet());
		druid.setUrlMappings(Arrays.asList("/druid/*"));//增加的拦截请求信息
        //初始化信息 用于登录用户名和密码
		HashMap<String,String> params = new HashMap<>();
		params.put("loginUsername", "admin");//用户名
		params.put("loginPassword", "admin");//密码
		druid.setInitParameters(params);
		return druid;
	}

	/**
	 * @see com.alibaba.druid.support.http.WebStatFilter
	 * @return
	 */
	@Bean
	public FilterRegistrationBean webStatFilter(){
		FilterRegistrationBean fitler = new FilterRegistrationBean();
		fitler.setFilter(new WebStatFilter());
		fitler.setUrlPatterns(Arrays.asList("/*"));
		//exclusions配置
		//经常需要排除一些不必要的url，比如.js,/jslib/等等。配置在init-param中。比如：
		fitler.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*,*.sql");
		return fitler;
	}
}
