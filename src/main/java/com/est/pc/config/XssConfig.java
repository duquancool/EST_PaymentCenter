package com.est.pc.config;

import com.est.pc.filter.CrosXssFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @Author:RyDu
 * @Date:2019/12/3 11:12
 * @Function:
 */
@Configuration
@SuppressWarnings("unchecked")  //标注不安全的类 过滤
public class XssConfig {
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean fitler = new FilterRegistrationBean();
        fitler.setFilter(new CrosXssFilter());
        fitler.setUrlPatterns(Arrays.asList("/*"));//具体拦截的url
        //exclusions配置
        //经常需要排除一些不必要的url，比如.js,/jslib/等等。配置在init-param中。比如：
        fitler.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*,*.sql");
        return fitler;
    }
}
