package com.est.pc.mapper;

import com.est.pc.domain.ModuleTask;

import java.util.List;
/**
 * @Author:Han
 * @Date:2019/12/9 16:09
 * @Function:模型层模块表接口类
 */
public interface ModuleTaskMapper {
    /**
     * findAll 查询全部
     */
    List<ModuleTask> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();
    /**
     *  save 保存信息
     * */
    void save(ModuleTask clazzInfo);
    /**
     * 查询一条修改记录
     * */
    ModuleTask toEdit(int id);
        //修改
    void edit(ModuleTask moduleTask);
        //根据id删除
    void delete(int id);
}