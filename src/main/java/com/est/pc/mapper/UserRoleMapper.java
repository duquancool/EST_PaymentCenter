package com.est.pc.mapper;

import com.est.pc.domain.UserRole;

import java.util.ArrayList;

/**
 * @Function:接口用户角色
 * @author: RyDu
 * @create: 2019-12-18 16:54
 **/
public interface UserRoleMapper {

    ArrayList<UserRole> findUserByRoleId(int id);

    void removeBindingUser(UserRole ur);

    ArrayList<UserRole> findNotUserByRoleId(int id);

    void addBindingUser(UserRole ur);
}