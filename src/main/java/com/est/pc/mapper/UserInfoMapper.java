package com.est.pc.mapper;

import com.est.pc.domain.UserInfoMiddle;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Function:持久层用户基本信息表
 * @author: Mr.Han
 * @create: 2019-12-11 13:42
 **/
public interface UserInfoMapper {
    /**
     * findAll  全部查询  根据 页数
     * @param pageNo
     */
    List<UserInfoMiddle> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

	/**
      *  不分页查询全部
	 */
    List<UserInfoMiddle> findAllNews();
    /**
     * 查询一条记录
     * @param id
     */
    UserInfoMiddle toEdit(int id);
    /**
     * 添加用户
     * @param clazzInfo
     */
    void add(UserInfoMiddle clazzInfo);

    /**
     * 修改用户
     * @param clazzInfo
     */
    void edit(UserInfoMiddle clazzInfo);

    /**
     * 删除用户
     * @param id
     */
    void delete(int id);

    /**
     * 更改密码
     * @param userId
     */
    void repass(@Param("userId") String userId, @Param("userPassword") String userPassword);

    /**
     * 添加学生
     * @param userInfoMiddle
     */
    int  addStudent(List<UserInfoMiddle> userInfoMiddle);

    /**
     * 查找最后一次插入的数据
     * @param i
     */
    List<Long> findlastInset(@Param("param1") int i,@Param("param2") int num);
}
