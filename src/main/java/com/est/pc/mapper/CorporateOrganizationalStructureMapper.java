package com.est.pc.mapper;

import com.est.pc.domain.CorporateOrganizationalStructure;

import java.util.List;
/**
 * @Author:RyDu
 * @Date:2019/12/20 17:27
 * @Function:公司组织结构表
 */
public interface CorporateOrganizationalStructureMapper {
    List<CorporateOrganizationalStructure> queryCompanyByTrainingModuleId(long courseTrainingModuleId);

    CorporateOrganizationalStructure findOne(int cosId);

    void addCorporateOrganizationalStructure(CorporateOrganizationalStructure cos);

    void editCorporateOrganizationalStructure(CorporateOrganizationalStructure cos);


}