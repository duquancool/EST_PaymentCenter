package com.est.pc.mapper;

import com.est.pc.domain.EvaluationSet;

/**
 * @Author:Han
 * @Date:2019/12/19
 * @Function:考评表接口类
 */
public interface EvaluationSetMapper {

    /**
     * 删除考评信息
     * **/
    void removeBindingEvaluation(int evaluationId);
    /**
     * 查找一条信息 用作比对
     * **/
   EvaluationSet findCourse_id(int course_id );
    /**
     * 删除一条记录
     * **/
    void addBindingEvaluation(EvaluationSet et);
    void editBindingEvaluation(EvaluationSet et);
}