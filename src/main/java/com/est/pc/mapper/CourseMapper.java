package com.est.pc.mapper;

import com.est.pc.domain.Course;
import com.est.pc.domain.CourseTrainingModule;

import java.util.List;
/**
 * @Author:Han
 * @Date:2019/12/19
 * @Function:课程表接口
 */
public interface CourseMapper {
    /**
     * findAll 查询全部
     */
    List<Course> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();
    /**
     *  save 保存信息
     * */
    void add(Course course);
    /**
     * 查询一条记录
     * */
    Course findOne(long id);

    void edit(Course course);

    void delete(int id);

    //查找所有的邀请码账号
    boolean findCourse_key(String course_key);

    /**
     * 创建所有的课程下模块绑定信息
     */
    int findOneByModuleCourse(long courseId);

    /**
     * 创建所有的课程下模块绑定信息
     */
    void addModulesByCourseId(CourseTrainingModule ctm);

    /**
     * 查询课程下的所有模块
     */
    List<CourseTrainingModule> queryModuleByCourseId(long courseId);


}