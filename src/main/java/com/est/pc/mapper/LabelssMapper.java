package com.est.pc.mapper;

import com.est.pc.domain.Labelss;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:14
 * @Function:标签模型层接口类
 */
public interface LabelssMapper {

    /**
     * findAll 查询全部
     */
    List<Labelss> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();

    /**
     * 查询全部
     */
    List<Labelss> findAllLabelss();

    /**
     * 查询一条记录
     */
    Labelss findOne(int id);

    /**
     * add 添加记录
     */
    void add(Labelss Labelss);

    /**
     * edit 修改
     */
    void edit(Labelss Labelss);

    /**
     * delete 删除记录
     */
    void delete(int id);
}
