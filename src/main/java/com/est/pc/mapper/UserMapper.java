package com.est.pc.mapper;

import com.est.pc.domain.UserInfo;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;

public interface UserMapper {
    @Select("select * from t_user where account=#{account}")
    UserInfo findByAccount(String account);



    @Select("select * from t_user ")
    ArrayList<UserInfo> findAll();


    @Select("select * from t_user where id=#{id}")
    UserInfo findById(int id);
}