package com.est.pc.mapper;

import com.est.pc.domain.RoleInfo;

import java.util.List;

/**
 * @Author:Han
 * @Date:2019/12/7 10:14
 * @Function:模型层角色表接口类
 */
public interface RoleInfoMapper {
    /**
     * 查询全部记录
     * @param pageNo
     * @return
     */
    List<RoleInfo> findAll(int pageNo);
    /**
     * 查询一条记录
     * @param id
     * @return
     */
    RoleInfo findOne(int id);

    /**
     * save
     * @paraminfo 增加
     */
    void add(RoleInfo roleInfo);

    /**
     * findCount 查询条数
     */
    int findCount();

   /**
     * edit 修改
     * @param roleInfo
     */
    void edit(RoleInfo roleInfo);

    /**
     * delete 删除
     * @param id
     */
    void delete(int id);


    /**
     * 查询全部
     * @return
     */
    List<RoleInfo> findAllNews();
}
