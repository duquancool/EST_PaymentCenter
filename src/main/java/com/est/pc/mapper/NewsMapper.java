package com.est.pc.mapper;

import com.est.pc.domain.NewsInfo;

import java.util.List;


public interface NewsMapper {

	/**
	 * findAll 查询全部
	 * @param pageNo
	 */
	List<NewsInfo> findAll(int pageNo);
	
	/**
	 * 查询一条记录
	 * @param id
	 * @return
	 */
	NewsInfo findOne(int id);
	
	/**
	 * save
	 * @param info 增加
	 */
	void save(NewsInfo info);
	
	/**
	 * findCount 查询条数
	 */
	int findCount();

	/**
	 * edit 修改
	 * @param info
	 */
	void edit(NewsInfo info);
	
	/**
	 * delete 删除
	 * @param id
	 */
	void delete(int id);


	/**
	 * 查询全部
	 * @return
	 */
	List<NewsInfo> findAllNews();
}
