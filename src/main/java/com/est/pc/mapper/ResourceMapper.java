package com.est.pc.mapper;

import com.est.pc.domain.Resources;

import java.util.List;

/**
 * @Function:持久层资源表接口类
 * @author: Mr.Han
 * @create: 2019-12-09 14:08
 **/
public interface ResourceMapper {
    /**
     * findAll  全部查询  根据 页数
     * @param pageNo
     */
    List<Resources> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();
//
    /**
     * 不分页查询全部
     */
    List<Resources> findAllNews();


//
    /**
     * 查询一条记录
     * @param resource_id 主键
     * @return
     */
    Resources toEdit(int resource_id);
//
    /**
     * save插入信息
     * @param resources
     */
    void save(Resources resources);
    /**
     * 修改信息
     * */
    void edit(Resources resources);
//
//	/**
//	 * edit
//	 * @param info
//	 */
//	void edit(NewsInfo info);
//
    /**
     * delete
     * @param id
     */
    void delete(int id);

}
