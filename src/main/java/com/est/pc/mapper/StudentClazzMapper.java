package com.est.pc.mapper;

import com.est.pc.domain.StudentClazz;

import java.util.List;

/**
 * @Author:HAN
 * @Date:2019/12/06 15:42
 * @Function:班级中间表接口
 */
public interface StudentClazzMapper {
    //查找某班级的学生
//
    List<Long> findAllNews(String page);
//
    /**
     * 查询一条记录
     * @param id
     * @return
     */
    StudentClazz toEdit(int id);
//
    /**
     * save
     * @param studentClazz
     */
    //添加用户
    void add(StudentClazz studentClazz);
    //修改用户
    void edit(StudentClazz studentClazz);
//
//	/**
//	 * edit
//	 * @param info
//	 */
//	void edit(NewsInfo info);
//
    /**
     * delete
     * @param id
     */
    //删除用户
    void delete(int id);
    //添加一组学生

    void addStudentAll(List<StudentClazz> studentClazz);

    void deleteStudent(int id);
}