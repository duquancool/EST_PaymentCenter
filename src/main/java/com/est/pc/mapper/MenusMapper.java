package com.est.pc.mapper;

import com.est.pc.domain.Menus;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 14:03
 * @Function:菜单模型层接口类
 */
public interface MenusMapper {
    /**
     * findAll 查询全部
     */
    List<Menus> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();

    /**
     * 查询全部
     */
    List<Menus> findAllMenus();

    /**
     * 查询一条记录
     */
    Menus findOne(int id);

    /**
     * 查询一条记录 去除当前ID的所有菜单
     */
    List<Menus> findAllMenusNotById(int id);

    /**
     * add 添加记录
     */
    void add(Menus menus);

    /**
     * edit 修改
     */
    void edit(Menus menus);

    /**
     * delete 删除记录
     */
    void delete(int id);
}