package com.est.pc.mapper;

import com.est.pc.domain.TrainingModule;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/20 10:41
 * @Function:模块接口类
 */
public interface TrainingModuleMapper {
    /**
     * findAll 查询全部
     */
    List<TrainingModule> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();

    /**
     * 查询全部
     */
    List<TrainingModule> findAllTrainingModule();

    /**
     * 查询一条记录
     */
    TrainingModule findOne(int id);

    /**
     * add 添加记录
     */
    void add( TrainingModule  trainingModule);

    /**
     * edit 修改
     */
    void edit( TrainingModule  trainingModule);

    /**
     * delete 删除记录
     */
    void delete(int id);
}