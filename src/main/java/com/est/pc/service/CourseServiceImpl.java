package com.est.pc.service;

import com.est.pc.domain.Course;
import com.est.pc.domain.CourseTrainingModule;
import com.est.pc.mapper.CourseMapper;
import com.est.pc.mapper.EvaluationSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Function:课程表业务接口实现类
 * @author: Mr.Han
 * @create: 2019-12-19 10:51
 **/
@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseMapper courseMapper;//班级持久层接口
    @Autowired
    private EvaluationSetMapper evaluationSetMapper;//课程表接口
    /**查询一组信息**/
    @Override
    public List<Course> findAll(int pageNo) {
        List<Course> courList = courseMapper.findAll(pageNo);
        for (Course c:courList){
          if(c.getCourseProfessional().equals("1")){}else{}

           if( c.getCourseStatus().equals("1")){
               c.setCourseStatus("开课");
           }else {
               c.setCourseStatus("未开课");
           }

        }
        return courList;
    }
    /**查找信息**/
    @Override
    public int findCount() {
        return courseMapper.findCount();
    }
    /**添加信息**/
    @Override
    public void add(Course course) {
        if(course.getCourseStartTime()==null) {
            course.setCourseStartTime(Calendar.getInstance().getTime());
        }
        Date courseStartTime = course.getCourseStartTime();
        Calendar instance = Calendar.getInstance();
        instance.setTime(courseStartTime);
        instance.add(instance.MONTH, 4 );//结束时间
        course.setCourseEndTime(instance.getTime());

        courseMapper.add(course);
    }

    @Override
    public Course findOne(long id) {
        return courseMapper.findOne(id);
    }
    /**
     * 修改
     * **/
    @Override
    public void edit(Course course) {
        Date courseStartTime = course.getCourseStartTime();
        Calendar instance = Calendar.getInstance();
        instance.setTime(courseStartTime);
        instance.add(Calendar.MONTH,4);//结束时间推到4个月后
        course.setCourseEndTime(instance.getTime());
        courseMapper.edit(course);
    }

    @Override
    public void delete(int id) {
        evaluationSetMapper.removeBindingEvaluation(id);
        courseMapper.delete(id);
    }
    /**
     * 查找表中的邀请码
     * **/
    @Override
    public boolean findCourse_key(String course_key) {
        return courseMapper.findCourse_key(course_key);
    }

    /**
     * 创建所有的课程下模块绑定信息
     */
    @Override
    public int findOneByModuleCourse(long courseId) {
        return courseMapper.findOneByModuleCourse(courseId);
    }

    /**
     * 创建所有的课程下模块绑定信息
     */
    @Override
    public void addModulesByCourseId(CourseTrainingModule ctm) {
        courseMapper.addModulesByCourseId(ctm);
    }

    /**
     * 查询课程下的所有模块
     */
    @Override
    public List<CourseTrainingModule> queryModuleByCourseId(long courseId) {
        return courseMapper.queryModuleByCourseId(courseId);
    }
}
