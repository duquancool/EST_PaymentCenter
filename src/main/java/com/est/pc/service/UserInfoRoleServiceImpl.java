package com.est.pc.service;

import com.est.pc.domain.UserRole;
import com.est.pc.mapper.UserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @Author:RyDu
 * @Date:2019/12/19 16:52
 * @Function:用户角色中间表业务实现类
 */
@Service
public class UserInfoRoleServiceImpl implements UserinfoRoleService {

    //用户角色中间表
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public ArrayList<UserRole> findUserByRoleId(int id) {
        return userRoleMapper.findUserByRoleId(id);
    }

    @Override
    public void removeBindingUser(UserRole ur) {
        userRoleMapper.removeBindingUser(ur);
    }

    @Override
    public ArrayList<UserRole> findNotUserByRoleId(int id) {
        return userRoleMapper.findNotUserByRoleId(id);
    }

    @Override
    public void addBindingUser(UserRole ur) {
        userRoleMapper.addBindingUser(ur);
    }
}
