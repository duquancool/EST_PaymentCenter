package com.est.pc.service;

import com.est.pc.domain.TrainingModule;
import com.est.pc.mapper.TrainingModuleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:17
 * @Function:
 */
@Service
public class TrainingModuleServiceImpl implements TrainingModuleService{

    @Autowired
    private TrainingModuleMapper trainingModuleMapper;

    /**
     * findAll 查询全部
     */
    @Override
    public List<TrainingModule> findAll(int pageNo) {
        return trainingModuleMapper.findAll(pageNo);
    }

    /**
     * findCount 查询条数
     */
    @Override
    public int findCount() {
        return trainingModuleMapper.findCount();
    }

    /**
     * findAllLabels 不分页 查询全部
     */
    @Override
    public List<TrainingModule> findAllTrainingModule() {
        return trainingModuleMapper.findAllTrainingModule();
    }

    /**
     * findOne 查询一条记录
     */
    @Override
    public TrainingModule findOne(int id) {
        return trainingModuleMapper.findOne(id);
    }

    /**
     * add 添加
     */
    @Transactional
    public void add(TrainingModule trainingModule) {
        trainingModuleMapper.add(trainingModule);
    }

    /**
     * edit 修改
     */
    @Override
    public void edit(TrainingModule trainingModule) {
        trainingModuleMapper.edit(trainingModule);
    }

    /**
     * delete 删除
     */
    @Override
    public void delete(int id) {
        trainingModuleMapper.delete(id);
    }

}
