package com.est.pc.service;

import com.est.pc.domain.Labelss;
import com.est.pc.mapper.LabelssMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:17
 * @Function:
 */
@Service
public class LaberssServiceImpl implements LaberssService{
    @Autowired
    private LabelssMapper laberssMapper;

    /**
     * findAll 查询全部
     */
    @Override
    public List<Labelss> findAll(int pageNo) {
        return laberssMapper.findAll(pageNo);
    }

    /**
     * findCount 查询条数
     */
    @Override
    public int findCount() {
        return laberssMapper.findCount();
    }

    /**
     * findAllLabels 不分页 查询全部
     */
    @Override
    public List<Labelss> findAllLabelss() {
        return laberssMapper.findAllLabelss();
    }

    /**
     * findOne 查询一条记录
     */
    @Override
    public Labelss findOne(int id) {
        return laberssMapper.findOne(id);
    }

    /**
     * add 添加
     */
    @Transactional
    public void add(Labelss labels) {
        laberssMapper.add(labels);
    }

    /**
     * edit 修改
     */
    @Override
    public void edit(Labelss labels) {
        laberssMapper.edit(labels);
    }

    /**
     * delete 删除
     */
    @Override
    public void delete(int id) {
        laberssMapper.delete(id);
    }

}
