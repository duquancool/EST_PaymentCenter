package com.est.pc.service;

import com.est.pc.domain.Menus;
import com.est.pc.mapper.MenusMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:17
 * @Function:菜单业务实现类
 */
@Service
public class MenusServiceImpl implements MenusService{
    @Autowired
    private MenusMapper menusMapper;

    /**
     * findAll 查询全部
     */
    @Override
    public List<Menus> findAll(int pageNo) {
        return menusMapper.findAll(pageNo);
    }

    /**
     * findCount 查询条数
     */
    @Override
    public int findCount() {
        return menusMapper.findCount();
    }

    /**
     * findAllLabels 不分页 查询全部
     */
    @Override
    public List<Menus> findAllMenus() {
        return menusMapper.findAllMenus();
    }

    /**
     * findOne 查询一条记录
     */
    @Override
    public Menus findOne(int id) {
        return menusMapper.findOne(id);
    }

    /**
     * findAllLabels 不分页 查询全部
     */
    @Override
    public List<Menus> findAllMenusNotById(int id) {
        return menusMapper.findAllMenusNotById(id);
    }

    /**
     * add 添加
     */
    @Transactional
    public void add(Menus labels) {
        menusMapper.add(labels);
    }

    /**
     * edit 修改
     */
    @Override
    public void edit(Menus labels) {
        menusMapper.edit(labels);
    }

    /**
     * delete 删除
     */
    @Override
    public void delete(int id) {
        menusMapper.delete(id);
    }

}
