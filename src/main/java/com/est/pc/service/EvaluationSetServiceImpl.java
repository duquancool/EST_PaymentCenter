package com.est.pc.service;

import com.est.pc.domain.EvaluationSet;
import com.est.pc.mapper.EvaluationSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Function:考评表业务接口处理类
 * @author: Mr.Han
 * @create: 2019-12-20 14:24
 **/
@Service
public class EvaluationSetServiceImpl implements EvaluationSetService {
    @Autowired
    private EvaluationSetMapper evaluationSetMapper;/**考评表接口**/

    @Override
    public void removeBindingEvaluation(int evaluationid) {
        evaluationSetMapper.removeBindingEvaluation(evaluationid);
    }

    @Override
    public EvaluationSet findCourse_id(int course_id) {

        return evaluationSetMapper.findCourse_id(course_id);
    }

    @Override
    public void addBindingEvaluation(EvaluationSet et) {
        evaluationSetMapper.addBindingEvaluation(et);
    }

    @Override
    public void editBindingEvaluation(EvaluationSet et) {

        System.out.println(et);
        evaluationSetMapper.editBindingEvaluation(et);
    }
}
