package com.est.pc.service;

import com.est.pc.domain.RoleMenus;
import com.est.pc.mapper.RoleMenusMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @Author:RyDu
 * @Date:2019/12/17 9:39
 * @Function:处理角色与菜单业务实现类
 */
@Service
public class RoleMenusServiceImpl  implements RoleMenusService{
    @Autowired
    private RoleMenusMapper roleMenusMapper;

    @Override
    public ArrayList<RoleMenus> findMenusByRoleId(int id) {
        return roleMenusMapper.findMenusByRoleId(id);
    }

    @Override
    public void removeBindingMenu(RoleMenus roleMenus) {
         roleMenusMapper.removeBindingMenu(roleMenus);
    }

    @Override
    public ArrayList<RoleMenus> findNotMenusByRoleId(int id) {
        return roleMenusMapper.findNotMenusByRoleId(id);
    }


    @Override
    public void addBindingMenu(RoleMenus roleMenus) {
        roleMenusMapper.addBindingMenu(roleMenus);
    }
}
