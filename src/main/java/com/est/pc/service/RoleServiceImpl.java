package com.est.pc.service;

import com.est.pc.domain.RoleInfo;
import com.est.pc.mapper.RoleInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:Han
 * @Date:2019/12/10 10:38
 * @Function:角色业务接口实现类
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleInfoMapper roleInfoMapper;
    @Override
    /**
     * findAll 查询全部
     */
    public List<RoleInfo> findAll(int pageNo) {
        List<RoleInfo> RoleList = roleInfoMapper.findAll(pageNo);
        return RoleList;
    }
    /**
     * findone 查询一条记录
     */
    @Override
    public RoleInfo findOne(int id) {
        return roleInfoMapper.findOne(id);
    }
    /**
     * save 保存记录
     */
    @Override
    public void add(RoleInfo roleInfo) {
        roleInfoMapper.add(roleInfo);
    }
    /**
     * findcount 查询条目数
     */
    @Override
    public int findCount() {
        return roleInfoMapper.findCount();
    }
    /**
     * edit 修改信息
     */
    @Override
    public void edit(RoleInfo roleInfo) {
        roleInfoMapper.edit(roleInfo);
    }
    /**
     * delete 删除信息根据id
     */
    @Override
    public void delete(int id) {
        roleInfoMapper.delete(id);
    }

    /**
     * findAllNews 查询全部记录
     */
    @Override
    public List<RoleInfo> findAllNews() {
        return roleInfoMapper.findAllNews();
    }
}
