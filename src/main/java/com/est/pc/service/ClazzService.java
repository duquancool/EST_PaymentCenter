package com.est.pc.service;

import com.est.pc.domain.ClazzInfo;
import com.est.pc.domain.UserInfoMiddle;

import java.util.List;
/**
 * @Author:Han
 * @Date:2019/12/10 10:38
 * @Function:班级业务接口
 */
public interface ClazzService {
    /**
     * findAll  全部查询  根据 页数
     * @param pageNo
     */
    List<ClazzInfo> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

	/**
	 * 查询一条记录
	 * @param id
	 * @return
	 */
	ClazzInfo toEdit(int id);
	/**
	 * save
	 * @param clazzInfo
     */
	void save(ClazzInfo clazzInfo);

    void edit(ClazzInfo clazzInfo);

	/**
	 * delete
	 * @param id
	 */
	void delete(int id);
	//插入
	public void insertUser(List<UserInfoMiddle> userInfoMiddles, String clazzid);
	//校验
	public List<String> checkList(List<UserInfoMiddle> userInfoMiddles,String clazzid);
}
