package com.est.pc.service;

import com.est.pc.domain.Resources;
import com.est.pc.mapper.ResourceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Function:资源业务接口实现类
 * @author: Mr.Han
 * @create: 2019-12-09 14:07
 **/
@Service
public class ResourcesServiceImpl implements ResourcesService {
    @Autowired
    private ResourceMapper resourceMapper;
    //查询 第几页的信息
    @Override
    public List<Resources> findAll(int pageNo) {
        List<Resources> all = resourceMapper.findAll(pageNo);




        return all;

    }

    @Override
    public int findCount() {
        return resourceMapper.findCount();
    }
    //查询全部信息
    @Override
    public List<Resources> findAllNews() {
        return resourceMapper.findAllNews();
    }
    //根据找到要修改的信息
    @Override
    public Resources toedit(int id) {
        return resourceMapper.toEdit(id);
    }
    //保存信息
    @Override
    public void save(Resources resources) {
        resourceMapper.save(resources);
    }
    //修改信息
    @Override
    public void edit(Resources resources) {
        resourceMapper.edit(resources);
    }
    //根据ID 删除信息
    @Override
    public void delete(int id) {
        resourceMapper.delete(id);
    }
}
