package com.est.pc.service;

import com.est.pc.domain.ClazzInfo;
import com.est.pc.domain.StudentClazz;
import com.est.pc.domain.UserInfoMiddle;
import com.est.pc.mapper.ClazzMapper;
import com.est.pc.mapper.StudentClazzMapper;
import com.est.pc.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/**
 * @Author:Han
 * @Date:2019/12/10 10:38
 * @Function:班级业务接口实现类
 */
@Service
public class ClazzServiceImpl implements ClazzService {

    @Autowired
    private ClazzMapper clazzMapper;//班级接口
    @Autowired
    private UserInfoMapper userInfoMapper;//用户接口
    @Autowired
    private StudentClazzMapper studentClazzMapper;//班级中间表接口
    /**
     * 查询全部信息
     * */
    @Override
    public List<ClazzInfo> findAll(int pageNo) {
        List<ClazzInfo> all = clazzMapper.findAll(pageNo);
        for (ClazzInfo c:all){
            String clazzisuser = c.getClazzisuser();

            if(clazzisuser.equals("1")){
                c.setClazzisuser("可用");
            }else{
                c.setClazzisuser("不可用");
            }
        }
        return all;
    }
    /**
     * 查询有多少记录
     * */
    @Override
    public int findCount() {
        return clazzMapper.findCount();
    }

    /**
     * 查询一条记录
     * */
    @Override
    public ClazzInfo toEdit(int id) {
        ClazzInfo oneclazz = clazzMapper.toEdit(id);
        return oneclazz;
    }
    //保存記錄 根據封装类添加
    @Override
    public void save(ClazzInfo clazzInfo) {
        //1代表开班
        clazzInfo.setClazzisuser("1");
        clazzMapper.save(clazzInfo);
    }
    //修改 根據封装类添加
    @Override
    public void edit(ClazzInfo clazzInfo) {
        //2代表该班级被修改
        clazzInfo.setClazzisuser("2");
        clazzMapper.eidt(clazzInfo);
    }
    //根据 班级ID 删除
    @Override
    public void delete(int id) {

        studentClazzMapper.delete(id);
        clazzMapper.delete(id);
    }
    //校验 判断是否有重复数据
    public List<String> checkList(List<UserInfoMiddle> userInfoMiddles,String clazzid){
        List<String> theSame=new ArrayList<>();//记录相同信息
        List<Long> allNews = studentClazzMapper.findAllNews(clazzid);
        boolean flag=true;
        int num=0;
        if(allNews.size()>0){
                for (UserInfoMiddle u:userInfoMiddles) {
                    for (Long l:allNews) {
                    if(l.longValue()==u.getUserStudentNumber().longValue()){
                        theSame.add("该班级中已经拥有了学号为"+u.getUserStudentNumber()+"的"+u.getUserRealname()+"同学");
                        flag=false;
                    }
                    if(u.getUserPassword()==null){
                        //给密码
                        u.setUserPassword("123456");
                    }
                }
            }

        }
        if (flag){
            insertUser(userInfoMiddles,clazzid);
        }

        return theSame;
    }
    //传递数据给数据库 通过foreach 将学生数据中的值传递给 用户表 和用户中间表 同时影响到班级的人数 需要在引入两个注入类
    public void insertUser(List<UserInfoMiddle> userInfoMiddles,String clazzid){

        int num=userInfoMapper.addStudent(userInfoMiddles);//插入用户基本表
        int count = userInfoMapper.findCount();//得到总条目数
        List<Long> student_id= userInfoMapper.findlastInset(count-num,num);//得到最近几条条插入记录的主键
        List<StudentClazz> studentList=new ArrayList<>();
       int numb=0;
        for (UserInfoMiddle c:userInfoMiddles){

            StudentClazz studentClazz=new StudentClazz();
            studentClazz.setStudentId(student_id.get(numb));//学生id
            //问题项 日后需调整
            studentClazz.setTeacherId(student_id.get(numb));//教师id
            studentClazz.setClazzId(Long.valueOf(clazzid));//班级id
            studentList.add(studentClazz);
            numb++;
        }
        ClazzInfo clazzInfo=new ClazzInfo();
        clazzInfo.setClazznumber(num);
        clazzInfo.setClazzid(Long.valueOf(clazzid));
        studentClazzMapper.addStudentAll(studentList);//给班级中间表添加学生
        clazzMapper.eidtClazz(clazzInfo);//更改班级表的人数
    }
}
