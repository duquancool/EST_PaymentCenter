package com.est.pc.service;

import com.est.pc.domain.NewsInfo;

import java.util.List;

public interface NewService {
	
	/**
	 * findAll
	 * @param pageNo
	 */
	List<NewsInfo> findAll(int pageNo);

	/**
	 * 不分页查询全部
	 */
	List<NewsInfo> findAllNews();
	
	/**
	 * findCount
	 */
	int findCount();
	
	/**
	 * 查询一条记录
	 * @param id
	 * @return
	 */
	NewsInfo findOne(int id);
	
	/**
	 * save
	 * @param info
	 */
	void save(NewsInfo info);

	/**
	 * edit
	 * @param info
	 */
	void edit(NewsInfo info);
	
	/**
	 * delete
	 * @param id
	 */
	void delete(int id);

	
}
