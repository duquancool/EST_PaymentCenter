package com.est.pc.service;

import com.est.pc.domain.CorporateOrganizationalStructure;
import com.est.pc.mapper.CorporateOrganizationalStructureMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/20 17:22
 * @Function:公司组织结构表实现类
 */
@Service
public class CorporateOrganizationalStructureServiceImpl implements CorporateOrganizationalStructureService {
    @Autowired
    private CorporateOrganizationalStructureMapper corporateOrganizationalStructureMapper;

    @Override
    public List<CorporateOrganizationalStructure> queryCompanyByTrainingModuleId(long courseTrainingModuleId) {
        return corporateOrganizationalStructureMapper.queryCompanyByTrainingModuleId(courseTrainingModuleId);
    }

    @Override
    public CorporateOrganizationalStructure findOne(int cosId) {
        return corporateOrganizationalStructureMapper.findOne(cosId);
    }

    @Override
    public void addCorporateOrganizationalStructure(CorporateOrganizationalStructure cos) {
        corporateOrganizationalStructureMapper.addCorporateOrganizationalStructure(cos);
    }

    @Override
    public void editCorporateOrganizationalStructure(CorporateOrganizationalStructure cos) {
        corporateOrganizationalStructureMapper.editCorporateOrganizationalStructure(cos);
    }
}
