package com.est.pc.service;

import com.est.pc.domain.RoleMenus;

import java.util.ArrayList;

/**
 * @Author:RyDu
 * @Date:2019/12/17 9:33
 * @Function:处理角色与菜单业务接口
 */
public interface RoleMenusService {

    /**
     * 查询已绑定的菜单信息
     * @param id
     * @return
     */
    ArrayList<RoleMenus> findMenusByRoleId(int id);

    /**
     * 查询未绑定的菜单信息
     * @param id
     * @return
     */
    ArrayList<RoleMenus> findNotMenusByRoleId(int id);

    /**
     * 移除已经绑定角色的菜单信息
     * @param menusId
     * @param roleId
     */
    void removeBindingMenu(RoleMenus roleMenus);

    /**
     * 添加角色绑定的菜单信息
     * @param menusId
     * @param roleId
     */
    void addBindingMenu(RoleMenus rm);


}
