package com.est.pc.service;

import com.est.pc.domain.TrainingModule;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/20 10:44
 * @Function:模块接口
 */
public interface TrainingModuleService {
    /**
     * findAll  全部查询  根据 页数
     */
    List<TrainingModule> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

    /**
     * 不分页查询全部
     */
    List<TrainingModule> findAllTrainingModule();

    /**
     * 查询一条记录
     */
    TrainingModule findOne(int id);
//
    /**
     * add 添加
     */
    void add(TrainingModule trainingModule);

    /**
     * edit 修改
     */
    void edit(TrainingModule trainingModule);

    /**
     * delete 删除
     */
    void delete(int id);
}
