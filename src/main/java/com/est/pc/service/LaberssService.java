package com.est.pc.service;

import com.est.pc.domain.Labelss;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:16
 * @Function:标签业务接口
 */
public interface LaberssService {
    /**
     * findAll  全部查询  根据 页数
     */
    List<Labelss> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

    /**
     * 不分页查询全部
     */
    List<Labelss> findAllLabelss();

    /**
     * 查询一条记录
     */
    Labelss findOne(int id);
//
    /**
     * add 添加
     */
    void add(Labelss Labelss);

    /**
     * edit 修改
     */
    void edit(Labelss Labelss);

    /**
     * delete 删除
     */
    void delete(int id);

}
