package com.est.pc.service;

import com.est.pc.domain.EvaluationSet;

/**
 * @Function:考评表业务接口
 * @author: Mr.Han
 * @create: 2019-12-20 14:23
 **/
public interface EvaluationSetService {
    /**
     * 删除考评信息
     * **/
    void removeBindingEvaluation(int course_id);
    /**
     * 查找一条信息 用作比对
     * **/
    EvaluationSet findCourse_id(int course_id );
    /**
     * 删除一条记录
     * **/
    void addBindingEvaluation(EvaluationSet et);
    void editBindingEvaluation(EvaluationSet et);
}
