package com.est.pc.service;

import com.est.pc.domain.CorporateOrganizationalStructure;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/20  17:20
 * @Function:公司组织结构接口
 */
public interface CorporateOrganizationalStructureService {
    /**
     * 查询课程下的公司信息
     * @param courseTrainingModuleId
     */
    List<CorporateOrganizationalStructure> queryCompanyByTrainingModuleId(long courseTrainingModuleId);

    /**
     * 查询公司自己的信息
     * @param cosId
     */
    CorporateOrganizationalStructure findOne(int cosId);

    /**
     * 添加公司信息
     * @param cos
     */
    void addCorporateOrganizationalStructure(CorporateOrganizationalStructure cos);

    /**
     * 修改公司信息
     * @param cos
     */
    void editCorporateOrganizationalStructure(CorporateOrganizationalStructure cos);

}
