package com.est.pc.service;

import com.est.pc.domain.Menus;

import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/16 14:00
 * @Function:菜单业务接口
 */
public interface MenusService {
    /**
     * findAll  全部查询  根据 页数
     */
    List<Menus> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

    /**
     * 不分页查询全部
     */
    List<Menus> findAllMenus();

    /**
     * 查询一条记录
     */
    Menus findOne(int id);

    /**
     * 查询一条记录 去除当前ID的所有菜单
     */
    List<Menus>  findAllMenusNotById(int id);
    /**
     * add 添加
     */
    void add(Menus menus);

    /**
     * edit 修改
     */
    void edit(Menus menus);

    /**
     * delete 删除
     */
    void delete(int id);

}
