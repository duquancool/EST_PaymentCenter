package com.est.pc.service;

import com.est.pc.domain.Resources;

import java.util.List;

/**
 * @Function:资源表的业务处理
 * @author: Mr.Han
 * @create: 2019-12-09 14:02
 **/
public interface ResourcesService {
    /**
     * findAll  全部查询  根据 页数
     * @param pageNo
     */
    List<Resources> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();
//
	/**
	 * 不分页查询全部
	 */
	List<Resources> findAllNews();


//
    /**
     * 查询一条记录
     * @param id
     * @return
     */
    Resources toedit(int id);
//
    /**
     * save插入信息
     * @param resources
     */
    void save(Resources resources);
/**
 * 修改信息
 * */
    void edit(Resources resources);
//
//	/**
//	 * edit
//	 * @param info
//	 */
//	void edit(NewsInfo info);
//
    /**
     * delete
     * @param id
     */
    void delete(int id);
}
