package com.est.pc.service;

import com.est.pc.domain.UserInfoMiddle;
import com.est.pc.mapper.StudentClazzMapper;
import com.est.pc.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @Function:
 * @author: Mr.Han
 * @create: 2019-12-11 13:46
 **/
@Service
public class UserInfoServiceImpl implements UserInfoService {

    //用户基本信息表
    @Autowired
    private UserInfoMapper userInfoMapper;
    //班级中间表
    @Autowired
    private StudentClazzMapper studentClazzMapper;

    @Override
    public List<UserInfoMiddle> findAll(int pageNo) {
        List<UserInfoMiddle> all = userInfoMapper.findAll(pageNo);
        for (UserInfoMiddle i: all) {
            String userSex = i.getUserSex();
            String userIsUser = i.getUserIsUser();
            if (userSex!=null){
            if(userSex.equals("1")){
                i.setUserSex("男");
            }else{
                i.setUserSex("女");
            }
            if(userIsUser.equals("1")){
                i.setUserIsUser("可用");
                }else {
                    i.setUserIsUser("不可用");
                }
        }}
        return all;
    }

    @Override
    public int findCount() {
        return userInfoMapper.findCount();
    }

    @Override
    public UserInfoMiddle toEdit(int id) {
        UserInfoMiddle userInfoMiddle = userInfoMapper.toEdit(id);
        String url="";
        try{
            url=userInfoMiddle.getUserImgurl().substring(userInfoMiddle.getUserImgurl().lastIndexOf("/upload"));
           }
        catch (Exception e){
        }
        userInfoMiddle.setUserImgurl("http://localhost:9000/images"+url);
        return userInfoMiddle ;
    }

    @Override
    public void add(UserInfoMiddle userInfoMiddle, HttpServletRequest request)throws IOException  {
        String uploadFileName = uploadFile(userInfoMiddle.getRealpath(), request);
        userInfoMiddle.setUserImgurl(uploadFileName);
        String userIsUser = userInfoMiddle.getUserIsUser();
        String replace = userIsUser.replace(",", "");
        String substring = replace.substring(0, 1);
        userInfoMiddle.setUserIsUser(substring );
        userInfoMapper.add(userInfoMiddle);}

    //上传文件的方法
    private static String uploadFile(MultipartFile multipartFile, HttpServletRequest request)throws IOException {
        String uploadPath="";
        String fileName="";
        if (multipartFile.getSize() > 0) {//判断文件大小
            String extendName = multipartFile.getOriginalFilename().substring(multipartFile.getOriginalFilename().lastIndexOf("."));//获取到后缀名
           fileName = UUID.randomUUID().toString() + extendName;//通过uuid 拼接图片名称
            String realPath = request.getServletContext().getRealPath("");//获取到项目的发布路径
             uploadPath = realPath.substring(0, realPath.indexOf("webapp\\")) + "resources/static/upload/userinfo/";
            FileCopyUtils.copy(multipartFile.getInputStream(), new FileOutputStream(new File(uploadPath + fileName)));//文件上传至该目录下
        }
        return uploadPath + fileName;
    }

    //修改文件
    @Override
    public void edit(UserInfoMiddle userInfoMiddle,HttpServletRequest request)throws IOException  {
        if(userInfoMiddle.getRealpath().getSize()>0){//如果文件的大小 大于0 则代表上传了新的图片
            String editFile = uploadFile(userInfoMiddle.getRealpath(), request);//得到上传的路径
            String realPath = request.getServletContext().getRealPath("");//获取到项目的发布路径
            String load=  realPath.substring(0, realPath.indexOf("webapp")) +"resources\\static"+userInfoMiddle.getUserImgurl();//原地址
            File file=new File(load);
            file.delete();//去除原图片
            userInfoMiddle.setUserImgurl(editFile);
        }
        String userIsUser = userInfoMiddle.getUserIsUser();
        String replace = userIsUser.replace(",", "");//接受radio数据时出现 1，问题 所以才去切割方式
        String substring = replace.substring(0, 1);
        userInfoMiddle.setUserIsUser(substring );
        userInfoMapper.edit(userInfoMiddle);
    }
    //用户的主键
    @Override
    public void delete(int id) {
        //删除他在角色基本表中的信息
        studentClazzMapper.deleteStudent(id);
        //删除他在用户基本信息表中的记录
        userInfoMapper.delete(id);
    }

    @Override
    public List<UserInfoMiddle> findAllNews() {
        return userInfoMapper.findAllNews();
    }

    @Override
    public void repass(String userId, String userPassword) {
        userInfoMapper.repass(userId,userPassword);
    }
}
