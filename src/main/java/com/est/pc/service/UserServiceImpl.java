package com.est.pc.service;

import com.est.pc.mapper.UserMapper;
import com.est.pc.domain.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	/**
	 * 根据账号Account查询当前用户
	 * @param account
	 * @return
	 */
	public UserInfo findByAccount(String account) {
		return userMapper.findByAccount(account);
	}


	/**
	 * 查询全部的数据信息
	 * @return
	 */
	@Override
	public ArrayList<UserInfo> findAll() {
		 return userMapper.findAll();
	}


	/**
	 * 根据id 查询具体的人员信息
	 * @param id
	 * @return
	 */
	@Override
	public UserInfo findById(int id) {
		return userMapper.findById(id);
	}


}