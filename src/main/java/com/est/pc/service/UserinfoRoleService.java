package com.est.pc.service;

import com.est.pc.domain.UserRole;

import java.util.ArrayList;

/**
 * @Author:RyDu
 * @Date:2019/12/19 16:46
 * @Function:用户角色中间表业务
 */
public interface UserinfoRoleService {
    /**
     * 通过角色查询所有的人员信息
     * @param id
     * @return
     */
    ArrayList<UserRole> findUserByRoleId(int id);

    /**
     * 单挑移除角色绑定的人员信息
     * @param userId
     * @param roleId
     */
    void removeBindingUser(UserRole ur);

    /**
     * 通过角色查询未分配的人员信息
     * @param id
     * @return
     */
    ArrayList<UserRole> findNotUserByRoleId(int id);

    /**
     * 绑定单条用户与角色信息
     * @param ur
     */
    void addBindingUser(UserRole ur);

}
