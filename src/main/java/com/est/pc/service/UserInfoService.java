package com.est.pc.service;

import com.est.pc.domain.UserInfoMiddle;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * @Function:
 * @author: Mr.Han
 * @create: 2019-12-11 13:46
 **/
public interface UserInfoService {
    /**
     * findAll  全部查询  根据 页数
     * @param pageNo
     */
    List<UserInfoMiddle> findAll(int pageNo);

    /**
     * findCount   查询一共有多少条数据
     */
    int findCount();

    /**
     * 查询一条记录
     * @param id
     * @return
     */
    UserInfoMiddle toEdit(int id);

    /**
     * save
     * @param userInfoMiddle
     */
    void add(UserInfoMiddle userInfoMiddle, HttpServletRequest request) throws IOException;

    void edit(UserInfoMiddle userInfoMiddle, HttpServletRequest request)throws IOException;
    /**
     * delete
     * @param id
     */
    void delete(int id);

    List<UserInfoMiddle> findAllNews();

    void repass(String userId, String userPassword);
}
