package com.est.pc;



import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages="com.est.pc.mapper")
//MyBaits 加载 配置文件的 路径  接口  对应的 Mapper文件的接口
public class SimpleApplication {
	public static void main(String[] args) {
		SpringApplication.run(SimpleApplication.class,args);
	}
}
