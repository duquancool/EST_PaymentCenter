package com.est.pc.web.ajax;

import com.est.pc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


/**
 * 本类是专门用来完成VueAjax数据请求的相应方法的
 * RestController 默认返回 json数据
 */
@Controller
//@CrossOrigin
public class UserController {
	@Autowired
	private UserService userService;

	/**
	 * userlist 返回json的数据请求信息
	 */
//	@ResponseBody
//	@RequestMapping(value={"userlist"})
//	public void userlist(HttpServletResponse res) throws IOException {
//		//获取全部的用户信息完成相应的数据查询
//		ArrayList<UserInfo>   userList=userService.findAll();
//		//通过Ajax完成数据的显示与查询
//		res.setCharacterEncoding("utf-8");
//		res.setHeader("Access-Control-Allow-Origin","*");//设置支持跨域请求地址
//		res.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");//设置支持的方法方式
//		PrintWriter  pw=res.getWriter();
//		pw.println( toJSONString(userList)); // List转json);
//		pw.close();
//	}

//	@RequestMapping(value={"useradd"})
//	public void useradd(@RequestParam("name")  String name,@RequestParam("account") String account, @RequestParam("password") String password,@RequestParam("createTime")  String createTime, HttpServletRequest req, HttpServletResponse res) throws IOException {
//		//通过传递的参数获取j'son的数据信息到后台进行接收数据信息
//		System.out.println("----------------------------");
//		System.out.println(name);
//		System.out.println(account);
//		System.out.println(password);
//		System.out.println(createTime);
//		System.out.println("----------------------------");
//
//		//通过Ajax完成数据的显示与查询
//		res.setCharacterEncoding("utf-8");
//		res.setHeader("Access-Control-Allow-Origin","http://localhost:8080");//设置支持跨域请求地址
//		res.setHeader("Access-Control-Allow-Methowds","PUT,POST,GET,DELETE,OPTIONS");//设置支持的方法方式
//		PrintWriter  pw=res.getWriter();
//		pw.println("OK"); // List转json);
//		pw.close();
//	}

//	@ResponseBody
//	@PostMapping(value={"useradd"},consumes = {"application/x-www-form-urlencoded"})
//	public void useradd(@RequestParam("name")  String name, @RequestParam("account") String account, 
//			@RequestParam("password") String password, @RequestParam("createTime")  String createTime)  {
//		//通过传递的参数获取j'son的数据信息到后台进行接收数据信息
//
//		System.out.println(name);
//		System.out.println(account);
//		System.out.println(password);
//	}
//	/**
//	 * 根据指定的userid ajax查询指定的数据信息
//	 */
//	@ResponseBody
//	@RequestMapping("user/{id}")
//	public void detail(HttpServletResponse res,@PathVariable int id) throws IOException {
////获取全部的用户信息完成相应的数据查询
//		UserInfo   userList=userService.findById(id);
//
//		//通过Ajax完成数据的显示与查询
//		res.setCharacterEncoding("utf-8");
//		res.setHeader("Access-Control-Allow-Origin","*");//设置支持跨域请求地址
//		res.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");//设置支持的方法方式
//		PrintWriter  pw=res.getWriter();
//		pw.println( toJSONString(userList)); // List转json);
//		pw.close();
//	}
}