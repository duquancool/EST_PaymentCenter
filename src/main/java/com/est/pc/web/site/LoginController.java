package com.est.pc.web.site;

import com.est.pc.domain.UserInfo;
import com.est.pc.service.UserService;
import com.est.pc.utils.EncryptUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/***********************
 * Author ：杜泉      
 * Day ：2019年7月26日
 * Time ：上午11:12:16
 * Functions:    LoginController.java
 **********************/

@Controller
public class LoginController {

	@Autowired
	private UserService userService;
	/**
	 * Login PostMethod
	 * @return
	 */
	@RequestMapping(value="login", method=RequestMethod.POST)
	public String login(HttpServletRequest request, RedirectAttributes rediect) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		
		
		System.out.println("用户名:"+account);
		System.out.println("密码:"+password);
		UserInfo info = userService.findByAccount(account);
		if(info == null) {
			rediect.addFlashAttribute("errorText", "该用户不存在");
			return "redirect:/login";
		}
		
		if(!StringUtils.equals(EncryptUtils.encryptMD5(password), info.getPassword())) {
			rediect.addFlashAttribute("errorText", "密码错误");
			return "redirect:/login";
		}
		
		request.getSession().setAttribute("sys_user_key", info);
		return "redirect:/admin/index";
	}
	
	
	@RequestMapping("admin/index")
	public String index() {
		return "login/login";
	}
}
