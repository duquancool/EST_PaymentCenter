package com.est.pc.web.site;

import com.est.pc.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author:RyDu
 * @Date:2019/11/30 15:15
 * @Function:
 */
@RestController
public class TestRedisController {

    @Autowired
    private RedisUtil redisUtil;

    //添加
    @GetMapping(value="/redisAdd")
    public void saveRedis(){
        redisUtil.set("id","123");
    }

    //获取
    @GetMapping(value="/redisGet")
    public String getRedis(){
        return redisUtil.get("id");
    }
}
