package com.est.pc.web.site;

import javax.servlet.http.HttpServletRequest;

import com.est.pc.domain.NewsInfo;
import com.est.pc.service.NewService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.est.pc.domain.PageInfo;

@Controller
public class IndexController {
	@Autowired
	private NewService newService;

	/**
	 * Index
	 * @param model
	 * @param request
	 */
	@RequestMapping(value={"","/","index"})
	public String index(Model model, HttpServletRequest request) {
		String page = request.getParameter("page");
		page = StringUtils.defaultIfBlank(page, "1");
		int pageNumber = Integer.valueOf(page);
		//查询所有的数据条数
		int total = newService.findCount();//findCount 查询个数
		//PageInfo  一个分页对象
		PageInfo<NewsInfo> pageInfo = new PageInfo<>(total, pageNumber);
		pageInfo.setList(newService.findAll(pageInfo.getPageStart()));//查询数据信息
		
		model.addAttribute("page", pageInfo);
		return "index";
	}

	/**
	 * demo
	 */
	@RequestMapping(value={"","/demo","demo"})
	public String demo(Model model, HttpServletRequest request) {
		model.addAttribute("message", "hello!!!!");
		return "demo";
	}
	
	/**
	 * Detail
	 * @param model
	 * @param id    url地址接收 传参  并在 方法的参数列表中完成 方法参数接值
	 */
	@RequestMapping("detail/{id}")
	public String detail(Model model, @PathVariable int id) {		
		model.addAttribute("info", newService.findOne(id));
		return "detail";
	}
}