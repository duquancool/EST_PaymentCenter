package com.est.pc.web.system;

import com.est.pc.domain.PageInfo;
import com.est.pc.domain.RoleInfo;
import com.est.pc.domain.RoleMenus;
import com.est.pc.domain.UserRole;
import com.est.pc.service.RoleMenusService;
import com.est.pc.service.RoleService;
import com.est.pc.service.UserinfoRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * @Author:Han
 * @Date:2019/12/7 15:43
 * @Function:角色控制层
 */
@Controller
@RequestMapping("system/admin/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleMenusService  roleMenusService;
    @Autowired
    private UserinfoRoleService userinfoRoleService;

    /**
     * 查询
     */
    @RequestMapping("list")
    public String list(HttpServletRequest request){
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);
        int total = roleService.findCount();
        PageInfo<RoleInfo> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(roleService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/roles/list.jsp";
    }

    /**
      *跳转到添加页面
     */
    @RequestMapping("toAdd")
    public String toAdd(){
        return "system/roles/add.jsp";
    }

    /**
     * 添加
     */
    @RequestMapping("add")
    public String save(RoleInfo RoleInfo){
        roleService.add(RoleInfo);
        return "redirect:/system/admin/role/list";
    }

    /**
     *跳转到修改页面
     */
    @RequestMapping("toEdit/{id}")
    public String toEdit(@PathVariable int id, HttpServletRequest request){
        request.setAttribute("role", roleService.findOne(id));
        return "system/roles/edit.jsp";
    }

    /**
     * 修改
     */
    @RequestMapping("edit")
    public String edit(RoleInfo roleInfo){
        roleService.edit(roleInfo);
        return "redirect:/system/admin/role/list";
    }

    /**
      * 删除
    */
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable int id){
        roleService.delete(id);
        return "redirect:/system/admin/role/list";
    }
    /**
     *跳转角色已绑定菜单查询页面
     */
    @RequestMapping("toQueryRoleBindingMenus/{id}")
    public String toQueryRoleBindingMenus(@PathVariable int id, HttpServletRequest request){
        //查询角色菜单中间表的数据信息  展示所有已经绑定的菜单信息
         ArrayList<RoleMenus> menusList=roleMenusService.findMenusByRoleId(id);
         request.setAttribute("menusList",menusList);//查询角色绑定的菜单
         request.setAttribute("roleId",id);//查询角色绑定的菜单
        return "system/roles/toBinding.jsp";
    }

    /**
     *单条移除绑定菜单
     */
    @RequestMapping("removeBindingMenu/{menuId}/{roleId}")
    public String removeBindingMenu(@PathVariable long menuId, @PathVariable long roleId){
        RoleMenus rm=new RoleMenus();
        rm.setMenuId(menuId);
        rm.setRoleId(roleId);
        roleMenusService.removeBindingMenu(rm);
        return "redirect:/system/admin/role/toQueryRoleBindingMenus/"+roleId;
    }

    /**
     *批量移除绑定菜单
     */
    @RequestMapping("removeBindingMenus/{roleId}/{menusIds}")
    public String addBindingMenu(@PathVariable long roleId, @PathVariable String menusIds){
        String[]   ids=menusIds.split("_");
        for(String menusId:ids){
            RoleMenus rm=new RoleMenus();
            rm.setMenuId(Long.parseLong(menusId));
            rm.setRoleId(roleId);
            roleMenusService.removeBindingMenu(rm);
        }
        return "redirect:/system/admin/role/toQueryRoleBindingMenus/"+roleId;
    }

    /**
     *跳转角色未绑定菜单查询页面
     */
    @RequestMapping("notBoundRoleBindingMenus/{id}")
    public String notBoundRoleBindingMenus(@PathVariable int id, HttpServletRequest request){
        //查询角色菜单中间表的数据信息  未绑定的菜单信息
        ArrayList<RoleMenus> menusList=roleMenusService.findNotMenusByRoleId(id);
        request.setAttribute("menusList",menusList);//查询角色未绑定的菜单
        request.setAttribute("roleId",id);//保存角色编号
        return "system/roles/notBoundList.jsp";
    }


    /**
     *单条绑定菜单查询页面
     */
    @RequestMapping("addBindingMenu/{menuId}/{roleId}")
    public String addBindingMenu(@PathVariable long menuId, @PathVariable long roleId){
        RoleMenus rm=new RoleMenus();
        rm.setMenuId(menuId);
        rm.setRoleId(roleId);
        roleMenusService.addBindingMenu(rm);
        return "redirect:/system/admin/role/notBoundRoleBindingMenus/"+roleId;
    }

    /**
     *批量绑定菜单
     */
    @RequestMapping("addBindingMenus/{roleId}/{menusIds}")
    public String addBindingMenus(@PathVariable long roleId, @PathVariable String menusIds){
        String[]   ids=menusIds.split("_");
        for(String menusId:ids){
            RoleMenus rm=new RoleMenus();
            rm.setMenuId(Long.parseLong(menusId));
            rm.setRoleId(roleId);
            roleMenusService.addBindingMenu(rm);
        }
        return "redirect:/system/admin/role/notBoundRoleBindingMenus/"+roleId;
    }
    /**
     *跳转角色已绑定人员查询页面
     */
    @RequestMapping("toRoleBindingUserinfo/{id}")
    public String toRoleBindingUserinfo(@PathVariable int id, HttpServletRequest request){
        //查询角色人员中间表的数据信息  展示所有已经绑定的人员信息
        ArrayList<UserRole> userList=userinfoRoleService.findUserByRoleId(id);
        request.setAttribute("userList",userList);//查询角色绑定的人员
        request.setAttribute("roleId",id);//保存角色编号
        return "system/roles/toUserBinding.jsp";
    }

    /**
     *单条移除绑定菜单
     */
    @RequestMapping("removeBindingUser/{userId}/{roleId}")
    public String removeBindingUser(@PathVariable long userId, @PathVariable long roleId){
        UserRole ur=new UserRole();
        ur.setUserId(userId);
        ur.setRoleId(roleId);
        userinfoRoleService.removeBindingUser(ur);
        return "redirect:/system/admin/role/toRoleBindingUserinfo/"+roleId;
    }

    /**
     *批量移除绑定人员
     */
    @RequestMapping("removeBindingUsers/{roleId}/{usersIds}")
    public String removeBindingUsers(@PathVariable long roleId, @PathVariable String usersIds){
        String[]   ids=usersIds.split("_");
        for(String userId:ids){
            UserRole ur=new UserRole();
            ur.setUserId(Long.parseLong(userId));
            ur.setRoleId(roleId);
            userinfoRoleService.removeBindingUser(ur);
        }
        return "redirect:/system/admin/role/toRoleBindingUserinfo/"+roleId;
    }

    /**
     *跳转用户分配角色页面
     */
    @RequestMapping("notRoleBindingUserinfo/{id}")
    public String notRoleBindingUserinfo(@PathVariable int id, HttpServletRequest request){
        //查询角色人员中间表的数据信息  未绑定的人员信息
        ArrayList<UserRole> userList=userinfoRoleService.findNotUserByRoleId(id);
        request.setAttribute("userList",userList);//查询角色未绑定的人员
        request.setAttribute("roleId",id);//保存角色编号
        return "system/roles/notUserBoundList.jsp";
    }

    /**
     *单条绑定人员与角色
     */
    @RequestMapping("addBindingUser/{userId}/{roleId}")
    public String addBindingUser(@PathVariable long userId, @PathVariable long roleId){
        UserRole ur=new UserRole();
        ur.setUserId(userId);
        ur.setRoleId(roleId);
        userinfoRoleService.addBindingUser(ur);
        return "redirect:/system/admin/role/notRoleBindingUserinfo/"+roleId;
    }

    /**
     *批量绑定人员分配角色
     */
    @RequestMapping("addBindingUsers/{roleId}/{userIds}")
    public String addBindingUsers(@PathVariable long roleId, @PathVariable String userIds){
        String[]   ids=userIds.split("_");
        for(String userId:ids){
            UserRole ur=new UserRole();
            ur.setUserId(Long.parseLong(userId));
            ur.setRoleId(roleId);
            userinfoRoleService.addBindingUser(ur);
        }
        return "redirect:/system/admin/role/notRoleBindingUserinfo/"+roleId;
    }
}
