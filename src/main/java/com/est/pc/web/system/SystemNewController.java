package com.est.pc.web.system;

import com.est.pc.domain.NewsInfo;
import com.est.pc.domain.PageInfo;
import com.est.pc.service.NewService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("system/admin/news/")
public class SystemNewController {
	@Autowired
	private NewService newService;

	/**
	 * List Table
	 * @param request
	 * @return
	 */
	@RequestMapping("list")
	public String list(HttpServletRequest request) {
		String page = request.getParameter("page");
		page = StringUtils.defaultIfBlank(page, "1");
		int pageNumber = Integer.valueOf(page);
		int total = newService.findCount();
		PageInfo<NewsInfo> pageInfo = new PageInfo<>(total, pageNumber);
		pageInfo.setList(newService.findAll(pageInfo.getPageStart()));
		request.setAttribute("page", pageInfo);
		return "system/news/list.jsp";
	}
	/**
	 * GO Add
	 * @return
	 */
	@RequestMapping("add")
	public String add() {
		return "system/news/add.jsp";
	}
	
	/**
	 * Save
	 * @return
	 */
	@RequestMapping("save")
	public String save(NewsInfo info) {
		newService.save(info);
		return "redirect:/system/admin/news/list"; 
	}
	
	/**
	 * Detail
	 * @param id
	 * @return
	 */
	@RequestMapping("detail/{id}")
	public String detail(@PathVariable int id, HttpServletRequest request) {
		request.setAttribute("info", newService.findOne(id));
		return "system/news/detail.jsp";
	}
	
	/**
	 * GO Edit
	 * @return
	 */
	@RequestMapping("toEdit/{id}")
	public String toEdit(@PathVariable int id,HttpServletRequest request) {
		//查询新闻信息到页面
		request.setAttribute("info", newService.findOne(id));
		return "system/news/edit.jsp";
	}
	
	/**
	 * Edit
	 * @return
	 */
	@RequestMapping("edit")
	public String edit(NewsInfo info) {
		newService.edit(info);
		return "redirect:/system/admin/news/list"; 
	}
	
	/**
	 * Delete
	 * @param id
	 * @return
	 */
	@RequestMapping("delete/{id}")
	public String delete(@PathVariable int id) {
		newService.delete(id);
		return "redirect:/system/admin/news/list"; 
	}
}
