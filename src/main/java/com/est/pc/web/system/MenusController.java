package com.est.pc.web.system;

import com.est.pc.domain.Menus;
import com.est.pc.domain.PageInfo;
import com.est.pc.service.MenusService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:RyDu
 * @Date:2019/12/16 13:59
 * @Function:菜单控制层
 */

@Controller
@RequestMapping("system/admin/menus/")
public class MenusController {

    @Autowired
    private MenusService menusService;

    /**
     * 查询全部标签
     */
    @RequestMapping("list")
    public String list(HttpServletRequest request) {
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);
        int total = menusService.findCount();
        PageInfo<Menus> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(menusService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/menus/list.jsp";
    }
    /**
     * 跳转到添加页面
     */
    @RequestMapping("toAdd")
    public String toAdd(HttpServletRequest request) {
        //查询菜单父节点信息到页面 进行下拉列表展示
        request.setAttribute("menusList", menusService.findAllMenus());
        return "system/menus/add.jsp";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping("add")
    public String add(Menus menus) {
        menusService.add(menus);
        return "redirect:/system/admin/menus/list";
    }

    /**
     * 跳转到修改
     * @return
     */
    @RequestMapping("toEdit/{id}")
    public String toEdit(@PathVariable int id, HttpServletRequest request) {
        //查询菜单父节点信息到页面 进行下拉列表展示 去除当前的id
        request.setAttribute("menusList", menusService.findAllMenusNotById(id));
        //查询新闻信息到页面
        request.setAttribute("menus", menusService.findOne(id));
        return "system/menus/edit.jsp";
    }

    /**
     * Edit
     * @return
     */
    @RequestMapping("edit")
    public String edit(Menus menus) {
        menusService.edit(menus);
        return "redirect:/system/admin/menus/list";
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable int id) {
        menusService.delete(id);
        return "redirect:/system/admin/menus/list";
    }
}
