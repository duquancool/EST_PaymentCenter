package com.est.pc.web.system;

import com.est.pc.domain.PageInfo;
import com.est.pc.domain.TrainingModule;
import com.est.pc.service.TrainingModuleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:RyDu
 * @Date:2019/12/20 10:43
 * @Function:模块控制层
 */
@Controller
@RequestMapping("system/admin/trainingModule/")
public class TrainingModuleController {

    @Autowired
    private TrainingModuleService trainingModuleService;

    /**
     * 查询全部模块
     */
    @RequestMapping("list")
    public String list(HttpServletRequest request) {
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);
        int total = trainingModuleService.findCount();
        PageInfo<TrainingModule> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(trainingModuleService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/training_module/list.jsp";
    }

    /**
     * 跳转到添加页面
     */
    @RequestMapping("toAdd")
    public String toAdd() {
        return "system/training_module/add.jsp";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping("add")
    public String add(TrainingModule trainingModule) {
        trainingModuleService.add(trainingModule);
        return "redirect:/system/admin/trainingModule/list";
    }

    /**
     * 跳转到修改
     * @return
     */
    @RequestMapping("toEdit/{id}")
    public String toEdit(@PathVariable int id, HttpServletRequest request) {
        //查询模块
        request.setAttribute("trainingModule",trainingModuleService.findOne(id));
        return "system/training_module/edit.jsp";
    }

    /**
     * Edit
     * @return
     */
    @RequestMapping("edit")
    public String edit(TrainingModule trainingModule) {
        trainingModuleService.edit(trainingModule);
        return "redirect:/system/admin/trainingModule/list";
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable int id) {
        trainingModuleService.delete(id);
        return "redirect:/system/admin/trainingModule/list";
    }
}
