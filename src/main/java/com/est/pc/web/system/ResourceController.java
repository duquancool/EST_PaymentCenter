package com.est.pc.web.system;

import com.est.pc.domain.PageInfo;
import com.est.pc.domain.Resources;
import com.est.pc.service.ResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Function:资源表控制层
 * @author: Mr.Han
 * @create: 2019-12-09 14:39
 **/
@Controller
@RequestMapping("/system/admin/resource")
public class ResourceController {
    @Autowired
    private ResourcesService resourcesService;

        //查询全部
        @RequestMapping("list")
        public String list(HttpServletRequest request){
            String page = request.getParameter("page");
            page = StringUtils.defaultIfBlank(page, "1");
            int pageNumber = Integer.valueOf(page);

            int total = resourcesService.findCount();
            PageInfo<Resources> pageInfo = new PageInfo<>(total, pageNumber);
            pageInfo.setList(resourcesService.findAll(pageInfo.getPageStart()));
            request.setAttribute("page", pageInfo);

            return "system/resources/list.jsp";
        }
        //添加
        @RequestMapping("add")
        public String add(Resources resources){
            resourcesService.save(resources);
            return "redirect:/system/admin/resource/list";
        }
        //跳转到添加页面
        @RequestMapping("toAdd")
        public String toAdd(HttpServletRequest request){
            return "system/resources/add.jsp";
        }

        @RequestMapping("toEdit/{id}")
        public String toEdit(@PathVariable("id") Integer resourceid, HttpServletRequest request){
            Resources resources = resourcesService.toedit(resourceid);
            request.setAttribute("page",resources);
            return "system/resources/edit.jsp";
        }
        //修改
        @RequestMapping("edit")
        public String updateClazz(Resources resources, HttpServletRequest request){
            resourcesService.edit(resources);
            return "redirect:/system/admin/resource/list";
        }
        //删除
        @RequestMapping("delete/{id}")
        public String delete(@PathVariable("id") Integer resourceid, HttpServletRequest request){
            resourcesService.delete(resourceid);
            return "redirect:/system/admin/resource/list";
        }
}
