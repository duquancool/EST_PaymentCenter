package com.est.pc.web.system;

import com.est.pc.domain.Labelss;
import com.est.pc.domain.PageInfo;
import com.est.pc.service.LaberssService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author:RyDu
 * @Date:2019/12/16 11:23
 * @Function:标签控制层
 */
@Controller
@RequestMapping("system/admin/labels/")
public class LabelssController {

    @Autowired
    private LaberssService laberssService;

    /**
     * 查询全部标签
     */
    @RequestMapping("list")
    public String list(HttpServletRequest request) {
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);

        int total = laberssService.findCount();
        PageInfo<Labelss> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(laberssService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/labelss/list.jsp";
    }
    /**
     * 跳转到添加页面
     */
    @RequestMapping("toAdd")
    public String toAdd() {
        return "system/labelss/add.jsp";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping("add")
    public String add(Labelss labelss) {
        laberssService.add(labelss);
        return "redirect:/system/admin/labels/list";
    }

    /**
     * 跳转到修改
     * @return
     */
    @RequestMapping("toEdit/{id}")
    public String toEdit(@PathVariable int id, HttpServletRequest request) {
        //查询标签到页面
        request.setAttribute("labels", laberssService.findOne(id));
        return "system/labelss/edit.jsp";
    }

    /**
     * Edit
     * @return
     */
    @RequestMapping("edit")
    public String edit(Labelss labelss) {
        laberssService.edit(labelss);
        return "redirect:/system/admin/labels/list";
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable int id) {
        laberssService.delete(id);
        return "redirect:/system/admin/labels/list";
    }
}
