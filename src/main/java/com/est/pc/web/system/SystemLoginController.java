package com.est.pc.web.system;

import com.est.pc.domain.UserInfo;
import com.est.pc.service.UserService;
import com.est.pc.utils.EncryptUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class SystemLoginController {
	@Autowired
	private UserService userService;
	
	/**
	 * Login GetMethod
	 * @return
	 */
	@RequestMapping(value="system/login", method=RequestMethod.GET)
	public String login() {
		return "system/login.jsp";
	}
	
	/**
	 * Login PostMethod
	 * @return
	 */
	@RequestMapping(value="system/login", method=RequestMethod.POST)
	public String login(HttpServletRequest request, RedirectAttributes rediect) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");

		//查询用户名是否存在
		UserInfo info = userService.findByAccount(account);
		if(info == null) {
			rediect.addFlashAttribute("errorText", "该用户不存在");
			return "redirect:/system/login";
		}
		//这块只验证的   密码 需要MD5加密 进行 验证
		if(!StringUtils.equals(EncryptUtils.encryptMD5(password), info.getPassword())) {
			rediect.addFlashAttribute("errorText", "密码错误");
			return "redirect:/system/login";
		}
		//保存信息到session中 用于 后台直接验证
		request.getSession().setAttribute("sys_user_key", info);
		return "redirect:/system/admin/index";
	}
	
	/**
	 * Exit
	 * @return
	 */
	@RequestMapping("system/logout")
	public String logout(HttpSession session) {
		session.invalidate();//清空 session信息
		return "redirect:/system/login";
	}
}