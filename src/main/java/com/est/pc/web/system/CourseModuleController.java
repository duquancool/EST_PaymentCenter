package com.est.pc.web.system;

import com.est.pc.domain.CorporateOrganizationalStructure;
import com.est.pc.domain.CourseTrainingModule;
import com.est.pc.domain.TrainingModule;
import com.est.pc.service.CorporateOrganizationalStructureService;
import com.est.pc.service.CourseService;
import com.est.pc.service.TrainingModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author:RyDu
 * @Date:2019/12/20 15:05
 * @Function:课程模块中间表控制层
 */
@Controller
@RequestMapping("system/admin/courseModule")
public class CourseModuleController {

    @Autowired
    private CourseService courseService;//课程
    @Autowired
    private TrainingModuleService trainingModuleService;//课程模块
    @Autowired
    private CorporateOrganizationalStructureService corporateOrganizationalStructureService;//公司组织结构模块

    /**
     * 通过课程查询关联表中的模块
     */
    @RequestMapping("queryModuleByCourseId/{courseId}")
    public String queryModuleByCourseId(@PathVariable long courseId, HttpServletRequest request) {
        //如果当前课程在关联表中没有关联数据 那么就 先插入多条数据进入 相应的中间表
        //1.先查询课程ID 中间表中是否存在
        int count = courseService.findOneByModuleCourse(courseId);

        if(0==count){
                //第一次开课 肯定没有就直接insert 多条数据到 中间表
                List<TrainingModule>  tmList= trainingModuleService.findAllTrainingModule();
                for(TrainingModule   tm:tmList){
                    //建议直接操作POJO 为后期的 模块邀请码  直接存值做准备
                    CourseTrainingModule  ctm=new CourseTrainingModule();
                    ctm.setCourseId(courseId);//课程编号
                    ctm.setTrainingModuleId(tm.getTrainingModuleId());//模块编号
                    ctm.setCourseTrainingModuleStatus(1);//模块可用状态
                    courseService.addModulesByCourseId(ctm);//保存课程与模块编号到中间表
                }
        }
        //2.如果有就直接查询  课程关联的模块信息进行  关联查询并展示
        //查询全部的模块信息  模块展示  展示到相应界面 并在list页面中直接 显示当前课程下的模块
        //查询标签到页面
        request.setAttribute("ctmList",courseService.queryModuleByCourseId(courseId));
        request.setAttribute("courseId",courseId);
        return "system/course_training_module/list.jsp";
    }
    /**
     * 查询课程某模块下的所有公司
     */
    @RequestMapping("queryCompanyByTrainingModuleId/{courseTrainingModuleId}")
    public String queryCompanyByTrainingModuleId(@PathVariable long courseTrainingModuleId, HttpServletRequest request) {
        request.setAttribute("cosList",corporateOrganizationalStructureService.queryCompanyByTrainingModuleId(courseTrainingModuleId));
        request.setAttribute("courseTrainingModuleId",courseTrainingModuleId);
        return "system/corporate_organizational_structure/list.jsp";
    }


    /**
     * 跳转到添加模块下公司名称
     * @return
     */
    @RequestMapping("toAddCorporateOrganizationalStructure/{courseTrainingModuleId}")
    public String toAddCorporateOrganizationalStructure(@PathVariable long courseTrainingModuleId, HttpServletRequest request) {
        request.setAttribute("courseTrainingModuleId",courseTrainingModuleId);//保存课程模块编号
        return "system/corporate_organizational_structure/add.jsp";
    }

    /**
     * 添加
     * @return
     */
    @RequestMapping("addCorporateOrganizationalStructure")
    public String addCorporateOrganizationalStructure(CorporateOrganizationalStructure cos) {
        System.out.println(cos.getCourseTrainingModuleId()+"--------111-------");
        System.out.println(cos.getCorporateOrganizationalStructureName()+"-----222----------");
        corporateOrganizationalStructureService.addCorporateOrganizationalStructure(cos);
        return "redirect:/system/admin/courseModule/queryCompanyByTrainingModuleId/"+cos.getCourseTrainingModuleId();
    }

    /**
     * 跳转到修改模块下公司名称
     * @return
     */
    @RequestMapping("toEditCorporateOrganizationalStructure/{cosId}")
    public String toEditCorporateOrganizationalStructure(@PathVariable int cosId, HttpServletRequest request) {
        //查询公司信息
        request.setAttribute("cos",corporateOrganizationalStructureService.findOne(cosId));
        return "system/corporate_organizational_structure/edit.jsp";
    }

    /**
     * 修改
     * @return
     */
    @RequestMapping("editCorporateOrganizationalStructure")
    public String editCorporateOrganizationalStructure(CorporateOrganizationalStructure cos) {
        corporateOrganizationalStructureService.editCorporateOrganizationalStructure(cos);
        return "redirect:/system/admin/courseModule/queryCompanyByTrainingModuleId/"+cos.getCourseTrainingModuleId();
    }
}
