package com.est.pc.web.system;

import com.est.pc.domain.PageInfo;
import com.est.pc.domain.UserInfoMiddle;
import com.est.pc.service.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Function:用户基本信息控制层
 * @author: Mr.Han
 * @create: 2019-12-11 09:38
 **/

@Controller
@RequestMapping("/system/admin/userInfo")
public class UserInfoController {
    /**上传地址*/

    @Autowired
    private UserInfoService userInfoService;
    //查询全部
    @RequestMapping("list")
    public String list(HttpServletRequest request){
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);

        int total = userInfoService.findCount();
        PageInfo<UserInfoMiddle> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(userInfoService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/user_info/list.jsp";
    }
    //添加
    @RequestMapping("add")
    public String add(UserInfoMiddle userInfoMiddle,HttpServletRequest request) throws IOException {
         userInfoService.add(userInfoMiddle,request);
        return "redirect:/system/admin/userInfo/list";
    }
    //跳转到添加页面
    @RequestMapping("toAdd")
    public String toAdd(HttpServletRequest request){
        return "system/user_info/add.jsp";
    }
    @RequestMapping("toEdit/{id}")
    public String toEdit(@PathVariable("id") Integer userId, HttpServletRequest request){
        UserInfoMiddle userInfoMiddle = userInfoService.toEdit(userId);
        request.setAttribute("page",userInfoMiddle);
        return "system/user_info/edit.jsp";
    }
    //修改
    @RequestMapping("edit")
    public String edit(UserInfoMiddle userInfoMiddle, HttpServletRequest request) throws IOException {
        userInfoService.edit(userInfoMiddle,request);
        return "redirect:/system/admin/userInfo/list";
    }
    //删除
    @RequestMapping("delete/{userId}")
    public String delete(@PathVariable("userId") Integer userId, HttpServletRequest request){
        userInfoService.delete(userId);
        return "redirect:/system/admin/userInfo/list";
    }
    @RequestMapping("repass/{userId}")
    public String Repass(@PathVariable("userId") Integer userId, HttpServletRequest request){
        UserInfoMiddle userInfoMiddle = userInfoService.toEdit(userId);
        System.out.println(userInfoMiddle);
        request.setAttribute("page",userInfoMiddle);
        return "/system/user_info/repass.jsp";
    }
    @RequestMapping("goRepass")
    public String goRepass(String userId,String userPassword){
        userInfoService.repass(userId,userPassword);
        return "redirect:/system/admin/userInfo/list";
    }
}
