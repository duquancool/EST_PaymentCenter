package com.est.pc.web.system;

import com.est.pc.domain.Course;
import com.est.pc.domain.EvaluationSet;
import com.est.pc.domain.PageInfo;
import com.est.pc.service.CourseService;
import com.est.pc.service.EvaluationSetService;
import com.est.pc.utils.RadomUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Function:课程表控制层
 * @author: Mr.Han
 * @create: 2019-12-19 10:55
 **/
@Controller
@RequestMapping("system/admin/course")
public class CourseController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private EvaluationSetService evaluationSetService;
    @RequestMapping("list")
    public String list(HttpServletRequest request){
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);
        int total = courseService.findCount();
        PageInfo<Course> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(courseService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/course/list.jsp";
    }

    //添加
    @RequestMapping("add")
    public String add(Course course){
        String radom = "";
        boolean flage=false;
        while (flage==true){
            radom=RadomUtil.getRadom();//得到6位邀请码
            flage=courseService.findCourse_key(radom);
        }
        course.setCourseKey(radom);//放入随机邀请码

        courseService.add(course);
        return "redirect:/system/admin/course/list";
    }

    //跳转到添加页面
    @RequestMapping("toAdd")
    public String toAdd(HttpServletRequest request){

        return "system/course/add.jsp";
    }
    /**转到修改页面**/
    @RequestMapping("toEdit/{courseid}")
    public String toEdit(@PathVariable Integer courseid, HttpServletRequest request){
        Course course = courseService.findOne(courseid);

        request.setAttribute("page",course);
        return "system/course/edit.jsp";
    }

    //修改
    @RequestMapping("edit")
    public String edit(Course course, HttpServletRequest request){

        courseService.edit(course);
        return "redirect:/system/admin/course/list";
    }

    /**删除**/
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable("id")  Integer courseid, HttpServletRequest request){
        courseService.delete(courseid);
        return "redirect:/system/admin/course/list";
    }
    @RequestMapping("toAddEn/{id}")
    public String toAddEn(@PathVariable("id") Integer courseid,HttpServletRequest request){
        request.setAttribute("courseid",courseid);
        request.setAttribute("type",2);//状态设置为添加
        return "system/course/evaluation_set.jsp";
    }
    @RequestMapping("addEn")
    public String toAddEn(EvaluationSet evaluationSet,Integer type, HttpServletRequest request){
        if(type==1){
            evaluationSetService.editBindingEvaluation(evaluationSet);
        }
        else{
            evaluationSetService.addBindingEvaluation(evaluationSet);
        }
        return "redirect:/system/admin/course/list";
    }
    @RequestMapping("editEn/{id}")
    public String editEn(@PathVariable("id") Integer courseid, HttpServletRequest request){
        EvaluationSet evaluation = evaluationSetService.findCourse_id(courseid);
        request.setAttribute("page",evaluation);
        request.setAttribute("type",1);//状态设置成修改
        return "system/course/evaluation_set.jsp";
    }

}
