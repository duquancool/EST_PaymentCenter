package com.est.pc.web.system;

import com.est.pc.domain.ClazzInfo;
import com.est.pc.domain.PageInfo;
import com.est.pc.domain.UserInfoMiddle;
import com.est.pc.service.ClazzService;
import com.est.pc.utils.ExcelUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("system/admin/clazz")
@SuppressWarnings("unchecked")
/**
 * @Author:Han
 * @Date:2019/12/7 14:43
 * @Function:班級控制层
 */
public class ClazzController {
    @Autowired
    private ClazzService clazzService;

    //查询全部
    @RequestMapping("list")
    public String list(HttpServletRequest request){
        String page = request.getParameter("page");
        page = StringUtils.defaultIfBlank(page, "1");
        int pageNumber = Integer.valueOf(page);

        int total = clazzService.findCount();
        PageInfo<ClazzInfo> pageInfo = new PageInfo<>(total, pageNumber);
        pageInfo.setList(clazzService.findAll(pageInfo.getPageStart()));
        request.setAttribute("page", pageInfo);
        return "system/clazzs/list.jsp";
    }

    //添加
    @RequestMapping("add")
    public String add(ClazzInfo clazzInfo){
        clazzService.save(clazzInfo);
        return "redirect:/system/admin/clazz/list";
    }

    //跳转到添加页面
    @RequestMapping("toAdd")
    public String toAdd(HttpServletRequest request){
        return "system/clazzs/add.jsp";
    }

    @RequestMapping("toEdit/{clazzid}")
    public String toEdit(@PathVariable Integer clazzid, HttpServletRequest request){
        ClazzInfo OneClazz = clazzService.toEdit(clazzid);
        request.setAttribute("page",OneClazz);
        return "system/clazzs/edit.jsp";
    }

    //修改
    @RequestMapping("edit")
    public String edit(ClazzInfo clazzInfo, HttpServletRequest request){
        clazzService.edit(clazzInfo);
        return "redirect:/system/admin/clazz/list";
    }

    //删除
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable("id")  Integer clazzid, HttpServletRequest request){
        clazzService.delete(clazzid);
        return "redirect:/system/admin/clazz/list";
    }
    @RequestMapping("uploadExcel")
    public String toUploadExcel(MultipartFile excel, String clazzid,HttpServletRequest request) throws IOException {
            Object o = ExcelUtils.execelToShopIdList(excel.getInputStream());
            List<UserInfoMiddle> userList=new ArrayList<>();//保存正确信息
            List<String> theSame=new ArrayList<>();//记录相同信息
        List<Map<String,Object>> errorsList=new ArrayList<>();//保存错误信息
        boolean flag=true;
            try {
                userList= (List<UserInfoMiddle>) o;
                 theSame = clazzService.checkList(userList, clazzid);
            }catch (ClassCastException c){
                flag=false;
                errorsList= ( List<Map<String,Object>>) o;
            }
            if (flag&&theSame.size()<=0){
                return "redirect:/system/admin/clazz/list";
            }else{
                request.setAttribute("clazzid",clazzid);
                request.setAttribute("error",errorsList);
                request.setAttribute("theSame",theSame);
                return "system/clazzs/mistake.jsp";
            }

    }
    @RequestMapping("toUploadExcel/{id}")
    public String toUploadExcel(@PathVariable("id") String clazzid,HttpServletRequest request){
            request.setAttribute("clazzid",clazzid);
            return "system/clazzs/upload.jsp";
    }

}
