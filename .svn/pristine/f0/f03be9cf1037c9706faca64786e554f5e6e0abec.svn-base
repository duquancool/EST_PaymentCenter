package com.est.pc.service;

import com.est.pc.domain.Course;
import com.est.pc.domain.CourseTrainingModule;

import java.util.List;

/**
 * @Function:课程表业务处理接口类
 * @author: Mr.Han
 * @create: 2019-12-19 10:49
 **/
public interface CourseService {
    /**
     * findAll 查询全部
     */
    List<Course> findAll(int pageNo);

    /**
     * findCount 查询条数
     */
    int findCount();

    /**
     *  save 保存信息
     * */
    void add(Course course);

    /**
     * 查询一条记录
     * */
    Course findOne(long id);

    /**
     * 修改
     * @param course
     */
    void edit(Course course);

    /**
     * 删除
     * @param id
     */
    void delete(int id);

    /**
     * 校验邀请码账号
     */
    boolean findCourse_key(String course_key);

    /**
     * 查询当前课程下是否创建了模块
     */
    int findOneByModuleCourse(long courseId);

    /**
     * 创建所有的课程下模块绑定信息
     */
    void addModulesByCourseId(CourseTrainingModule ctm);

    /**
     * 查询课程下的所有模块
     */
    List<CourseTrainingModule> queryModuleByCourseId(long courseId);

}
