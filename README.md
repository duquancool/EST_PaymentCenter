# spring-boot-freemarker, 依赖spring-boot-parent
* [spring-boot](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
* [freemarker](http://freemarker.org/)

        http://localhost:9000/ 前台展示目录
	 	http://localhost:9000/system/login 后台登录地址
> * http://localhost/system/login展示后台发布新闻列表
> * http://localhost/前端Freemark模板展示
> * 中间用到了Mysql, Mybatis, druid;可以参考spring-boot相关的demo


1.项目搭建是用 Maven+JDK1.8环境进行使用
2.项目代码采用SpringBoot+Maven3+MyBaits+FreeMarker进行使用 数据库采用 Mysql 数据库文件在 /src/main/resources/init-sql/schema.sql 为导入文件
3.项目结构
		java目录下 
				config     ：   配置文件目录
				                        Druid 为数据库连接池技术
				domain   ：  pojo实体类对象
				interceptor ： 拦截器目录    用于判断用户session状态的
				mappper ： Dao  - Mybaits 映射文件 为封装的接口方法
				service ：这里是 业务层 只要衔接 控制层Controller 与 Dao层之间的衔接  包含接口与实现类
				utils :工具类
				web : site  前台的Controller
						  system  后台的Controller


				SimpleApplication.java  springboot 的启动入口
		resources目录下
				init -sql： 初始化项目的sql文件
				mapper: mybaits 的 xml文件 接口 映射文件
				static :  静态的模版与插件
				templates : freemarker 模版类的 ftl文件 主要用于生成 页面进行使用
				application.properties 文件 是 项目中 的主要配置文件 用于配置具体的方法参数（freemarker   jdbc  ...）
						spring.freemarker.order=0    使用Maven时 可以不用管这个属性
						会导致jsp 与 ftl 文件不能互相访问
						所以 访问jsp时需要打开路径
				log4j2.xml  是日志文件用于配置 生成项目中的日志  暂未使用
	
	 webapp 目录下
	 	为前台的 页面显示目录 所有页面直接生成在 WEB_INF目录下  views为页面目录
	 	site 为前台的显示目录 jsp
	 	system 为后台的显示目录 jsp 通过 ftl 生成jsp


